//
//  UserModel.m
//  Funhub
//
//  Created by Ahmed Sadiq on 23/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.country forKey:@"country"];
    [encoder encodeObject:self.created_date forKey:@"created_date"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.first_name forKey:@"first_name"];
    [encoder encodeObject:self.userID forKey:@"userID"];
    [encoder encodeObject:self.imatrix_id forKey:@"imatrix_id"];
    [encoder encodeObject:self.last_name forKey:@"last_name"];
    [encoder encodeObject:self.password forKey:@"password"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.referrer_id forKey:@"referrer_id"];
    [encoder encodeObject:self.user_name forKey:@"user_name"];
    [encoder encodeObject:self.user_type forKey:@"user_type"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.country = [decoder decodeObjectForKey:@"country"];
        self.created_date = [decoder decodeObjectForKey:@"created_date"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.first_name = [decoder decodeObjectForKey:@"first_name"];
        self.userID = [decoder decodeObjectForKey:@"userID"];
        self.imatrix_id = [decoder decodeObjectForKey:@"imatrix_id"];
        self.last_name = [decoder decodeObjectForKey:@"last_name"];
        self.password = [decoder decodeObjectForKey:@"password"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.referrer_id = [decoder decodeObjectForKey:@"referrer_id"];
        self.user_name = [decoder decodeObjectForKey:@"user_name"];
        self.user_type = [decoder decodeObjectForKey:@"user_type"];
    }
    return self;
}



@end
