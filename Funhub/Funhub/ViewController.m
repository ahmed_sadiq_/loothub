//
//  ViewController.m
//  Funhub
//
//  Created by Hannan Khan on 11/11/15.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "ViewController.h"
#import "HomeVC.h"
#import "Game.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "SharedManager.h"
#import "NavigationHandler.h"
#import "AppDelegate.h"

#import <QuartzCore/QuartzCore.h>

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    tapper.delegate = self;
    [self.view addGestureRecognizer:tapper];
    
    countryCode = @"USA";
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: _forgotPswdText.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0.701 green:0.913 blue:0.286 alpha:1.0]
                 range:NSMakeRange(21, 9)];
    [_forgotPswdText setAttributedText: text];
    
    NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc] initWithAttributedString: _signUpTxt.attributedText];
    
    [text2 addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:0.701 green:0.913 blue:0.286 alpha:1.0]
                 range:NSMakeRange(23, 8)];
    [_signUpTxt setAttributedText: text2];
    
    NSMutableAttributedString *text3 = [[NSMutableAttributedString alloc] initWithAttributedString: _SignInTxt.attributedText];
    [text3 addAttribute:NSForegroundColorAttributeName
                  value:[UIColor colorWithRed:0.701 green:0.913 blue:0.286 alpha:1.0]
                  range:NSMakeRange(25, 8)];
    [_SignInTxt setAttributedText: text3];
    
    //_forgotPswdText.text = string;
    
    _countrySearchView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _countrySearchView.layer.borderWidth = 1.0f;
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIControl class]]) {
        // we touched a button, slider, or other UIControl
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInPressed:(id)sender {
    [self clearSignInfields];
    if(_logInEmail.text.length > 0 && _logInPswd.text.length > 0) {
        [CustomLoading showAlertMessage];
        [self sendLoginCall];
    }
    else {
        
        if(_logInEmail.text.length < 1) {
            _logInEmail.layer.borderColor = [UIColor redColor].CGColor;
            _logInEmail.layer.borderWidth = 1.0f;
        }
        else if(_logInPswd.text.length < 1) {
            _logInPswd.layer.borderColor = [UIColor redColor].CGColor;
            _logInPswd.layer.borderWidth = 1.0f;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"All fields should be filled with valid values." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void) clearSignInfields {
    
    _logInEmail.layer.borderColor = [UIColor clearColor].CGColor;
    _logInEmail.layer.borderWidth = 1.0f;
    
    _logInPswd.layer.borderColor = [UIColor clearColor].CGColor;
    _logInPswd.layer.borderWidth = 1.0f;
    
}


- (IBAction)skipPressed:(id)sender {
}

- (IBAction)moveToSignUp:(id)sender {
    _signUpView.hidden = false;
    _logInView.hidden = true;
}

- (IBAction)signUpPressed:(id)sender {
    
    [self makeAllSignUpFieldsClear];
    
    if([self validateEmail:_signUpEmail.text] && _signUpFName.text.length > 0 &&_signUpLName.text.length > 0 && _signUpUName.text.length > 0 && _signUpPswd.text.length > 5) {
        
        [CustomLoading showAlertMessage];
        
        [self sendSignUpCall];
    }
    else {
        
        
        
        if(![self validateEmail:_signUpEmail.text] ) {
            _signUpEmail.layer.borderColor = [UIColor redColor].CGColor;
            _signUpEmail.layer.borderWidth = 1.0f;
        }
        else if(_signUpFName.text.length < 1 ) {
            _signUpFName.layer.borderColor = [UIColor redColor].CGColor;
            _signUpFName.layer.borderWidth = 1.0f;
        }
        else if(_signUpLName.text.length < 1 ) {
            _signUpLName.layer.borderColor = [UIColor redColor].CGColor;
            _signUpLName.layer.borderWidth = 1.0f;
        }
        else if(_signUpUName.text.length < 1 ) {
            _signUpUName.layer.borderColor = [UIColor redColor].CGColor;
            _signUpUName.layer.borderWidth = 1.0f;
        }
        else if(_signUpPswd.text.length < 5 ) {
            _signUpPswd.layer.borderColor = [UIColor redColor].CGColor;
            _signUpPswd.layer.borderWidth = 1.0f;
        }
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"All fields should be filled with valid values." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void) makeAllSignUpFieldsClear {
    _signUpEmail.layer.borderColor = [UIColor clearColor].CGColor;
    _signUpEmail.layer.borderWidth = 1.0f;
    
    _signUpFName.layer.borderColor = [UIColor clearColor].CGColor;
    _signUpFName.layer.borderWidth = 1.0f;
    
    _signUpLName.layer.borderColor = [UIColor clearColor].CGColor;
    _signUpLName.layer.borderWidth = 1.0f;
    
    _signUpUName.layer.borderColor = [UIColor clearColor].CGColor;
    _signUpUName.layer.borderWidth = 1.0f;
    
    _signUpPswd.layer.borderColor = [UIColor clearColor].CGColor;
    _signUpPswd.layer.borderWidth = 1.0f;
}

- (IBAction)moveToSignIn:(id)sender {
    _signUpView.hidden = true;
    _logInView.hidden = false;
}

- (IBAction)signUpSkippedPressed:(id)sender {
}

- (IBAction)countryPressed:(id)sender {
    _signUpCPicker.hidden = false;
    _cPickerDoneBtn.hidden = false;
    
    _countrySearchView.hidden = false;
    
}

- (IBAction)cPickerDoneBtnPressed:(id)sender {
    _signUpCPicker.hidden = true;
    _cPickerDoneBtn.hidden = true;
    _countrySearchView.hidden = true;
}

- (IBAction)forgotPswdPressed:(id)sender {
    _forgotPswdView.hidden = false;
}

- (IBAction)resetPswdPressed:(id)sender {
    //_forgotPswdView.hidden = true;
    
    
    
    if(_remindUserNameTxt.text.length > 0) {
        [self sendForgotCall:_remindUserNameTxt.text];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"User Name cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}


- (IBAction)remindMePressed:(id)sender {
    //_forgotPswdView.hidden = true;
    
    if([self validateEmail:_remindEmailTxt.text]) {
        [self sendForgotCall:_remindEmailTxt.text];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email is not in valid format" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)pswdSkipPressed:(id)sender {
    _forgotPswdView.hidden = true;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag > 10) {
        [self animateTextField: textField up: YES];
    }
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag > 10) {
        [self animateTextField: textField up: NO];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - Country Picket Delegate Methods
- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _signUpCountry.text = name;
    countryCode = code;
}

#pragma mark - Server Communication Methods

-(void) sendLoginCall {
    
    self.view.frame = CGRectOffset(self.view.frame, 0, 0);
    [self.view endEditing:YES];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_LOGIN forKey:@"method"];
    [postParams setObject:_logInEmail.text forKey:@"email"];
    [postParams setObject:_logInPswd.text forKey:@"password"];
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSDictionary *userData = [result objectForKey:@"user_data"];
                
                UserModel *userTempModel = [[UserModel alloc] init];
                
                userTempModel.userID = [userData objectForKey:@"id"];
                userTempModel.email = [userData objectForKey:@"email"];
                userTempModel.phone = [userData objectForKey:@"phone"];
                userTempModel.country = [userData objectForKey:@"country"];
                userTempModel.password = [userData objectForKey:@"password"];
                userTempModel.last_name = [userData objectForKey:@"last_name"];
                userTempModel.user_name = [userData objectForKey:@"user_name"];
                userTempModel.user_type = [userData objectForKey:@"user_type"];
                userTempModel.first_name = [userData objectForKey:@"first_name"];
                userTempModel.imatrix_id = [userData objectForKey:@"imatrix_id"];
                userTempModel.referrer_id = [userData objectForKey:@"referrer_id"];
                userTempModel.created_date = [userData objectForKey:@"created_date"];
                userTempModel.referrer_user_name = [userData objectForKey:@"referrer_user_name"];
                userTempModel.balance = [userData objectForKey:@"imatrix_total_tokens"];
                
                [[NSUserDefaults standardUserDefaults] setObject:userTempModel.balance forKey:@"imatrix_total_tokens"];
                
                [[NSUserDefaults standardUserDefaults] setObject:userTempModel.referrer_user_name forKey:@"referrerName"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [SharedManager shareManager].user = userTempModel;
                [SharedManager shareManager].sessionToken = [result objectForKey:@"session_token"];
                
                [[SharedManager shareManager] saveCustomObject:userTempModel key:@"userTempModel"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"session_token"] forKey:@"session_token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSMutableArray *gamesToBeAdded = [[NSMutableArray alloc] init];
                
                NSArray *gamesArray = [result objectForKey:@"games_data"];
                for(int i=0; i<gamesArray.count; i++) {
                    NSDictionary *tempDict = [gamesArray objectAtIndex:i];
                    Game *gameObj = [[Game alloc] init];
                    gameObj.android_download_link = [tempDict objectForKey:@"android_download_link"];
                    gameObj.category_id = [tempDict objectForKey:@"category_id"];
                    gameObj.category_name = [tempDict objectForKey:@"category_name"];
                    gameObj.deep_link = [tempDict objectForKey:@"deep_link"];
                    gameObj.gameDesc = [tempDict objectForKey:@"description"];
                    gameObj.game_icon = [tempDict objectForKey:@"game_icon"];
                    gameObj.game_id = [tempDict objectForKey:@"game_id"];
                    gameObj.gln_game_id = [tempDict objectForKey:@"gln_game_id"];
                    gameObj.game_name = [tempDict objectForKey:@"game_name"];
                    gameObj.isFeatured = [[tempDict objectForKey:@"is_featured"] boolValue];
                    gameObj.game_video_link = [tempDict objectForKey:@"game_video_link"];
                    gameObj.ios_download_link = [tempDict objectForKey:@"ios_download_link"];
                    gameObj.created_date = [tempDict objectForKey:@"created_date"];
                    gameObj.video_thumbnail = [tempDict objectForKey:@"video_thumbnail_link"];
                    gameObj.banner_url = [tempDict objectForKey:@"referral_deep_link"];
                    gameObj.featured_image = [tempDict objectForKey:@"featured_graphic_link"];
                    gameObj.screen_shots = [[NSMutableArray alloc] init];
                    
                    NSArray *screenshots = [tempDict objectForKey:@"screen_shots"];
                    for(int j=0; j<screenshots.count; j++) {
                        NSString *imageUrl = [screenshots objectAtIndex:j];
                        [gameObj.screen_shots addObject:imageUrl];
                    }
                    
                    [gamesToBeAdded addObject:gameObj];
                    
                }
                
                [SharedManager shareManager].games = gamesToBeAdded;
                
                [[NavigationHandler getInstance] NavigateToHomeScreen];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verification Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

-(void) sendSignUpCall {
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_SIGNUP forKey:@"method"];
    [postParams setObject:_signUpEmail.text forKey:@"email"];
    [postParams setObject:_signUpPswd.text forKey:@"password"];
    [postParams setObject:_signUpFName.text forKey:@"first_name"];
    [postParams setObject:_signUpLName.text forKey:@"last_name"];
    
    NSString *referID = [[NSUserDefaults standardUserDefaults] objectForKey:@"referrer_imatrix_id"];
    
    if(referID.length > 0) {
        NSLog(@"Referral Id found!!!! YUPIEEEEEEEE");
        [postParams setObject:referID forKey:@"referrer_imatrix_id"];
    }
    else {
        [postParams setObject:@"1586701" forKey:@"referrer_imatrix_id"];
    }
    [postParams setObject:_signUpUName.text forKey:@"user_name"];
    [postParams setObject:countryCode forKey:@"country_code"];
    if(_signUpPhone.text.length <= 1) {
        [postParams setObject:@"1111111" forKey:@"phone"];
    }
    else {
        [postParams setObject:_signUpPhone.text forKey:@"phone"];
    }
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSDictionary *userData = [result objectForKey:@"user_data"];
                
                UserModel *userTempModel = [[UserModel alloc] init];
                
                userTempModel.userID = [userData objectForKey:@"id"];
                userTempModel.email = [userData objectForKey:@"email"];
                userTempModel.phone = [userData objectForKey:@"phone"];
                userTempModel.country = [userData objectForKey:@"country"];
                userTempModel.password = [userData objectForKey:@"password"];
                userTempModel.last_name = [userData objectForKey:@"last_name"];
                userTempModel.user_name = [userData objectForKey:@"user_name"];
                userTempModel.user_type = [userData objectForKey:@"user_type"];
                userTempModel.first_name = [userData objectForKey:@"first_name"];
                userTempModel.imatrix_id = [userData objectForKey:@"imatrix_id"];
                userTempModel.referrer_id = [userData objectForKey:@"referrer_id"];
                userTempModel.created_date = [userData objectForKey:@"created_date"];
                userTempModel.referrer_user_name = [userData objectForKey:@"referrer_user_name"];
                userTempModel.balance = [userData objectForKey:@"imatrix_total_tokens"];
                
                [SharedManager shareManager].user = userTempModel;
                [SharedManager shareManager].sessionToken = [result objectForKey:@"session_token"];
                [[SharedManager shareManager] saveCustomObject:userTempModel key:@"userTempModel"];
                
                [[NSUserDefaults standardUserDefaults] setObject:userTempModel.referrer_user_name forKey:@"referrerName"];
                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"session_token"] forKey:@"session_token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:userTempModel.balance forKey:@"imatrix_total_tokens"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSMutableArray *gamesToBeAdded = [[NSMutableArray alloc] init];
                
                NSArray *gamesArray = [result objectForKey:@"games_data"];
                for(int i=0; i<gamesArray.count; i++) {
                    NSDictionary *tempDict = [gamesArray objectAtIndex:i];
                    Game *gameObj = [[Game alloc] init];
                    gameObj.android_download_link = [tempDict objectForKey:@"android_download_link"];
                    gameObj.category_id = [tempDict objectForKey:@"category_id"];
                    gameObj.category_name = [tempDict objectForKey:@"category_name"];
                    gameObj.deep_link = [tempDict objectForKey:@"deep_link"];
                    gameObj.gameDesc = [tempDict objectForKey:@"description"];
                    gameObj.game_icon = [tempDict objectForKey:@"game_icon"];
                    gameObj.game_id = [tempDict objectForKey:@"game_id"];
                    gameObj.gln_game_id = [tempDict objectForKey:@"gln_game_id"];
                    gameObj.game_name = [tempDict objectForKey:@"game_name"];
                    gameObj.isFeatured = [[tempDict objectForKey:@"is_featured"] boolValue];
                    
                    gameObj.game_video_link = [tempDict objectForKey:@"game_video_link"];
                    gameObj.ios_download_link = [tempDict objectForKey:@"ios_download_link"];
                    gameObj.created_date = [tempDict objectForKey:@"created_date"];
                    gameObj.video_thumbnail = [tempDict objectForKey:@"video_thumbnail_link"];
                    gameObj.featured_image = [tempDict objectForKey:@"featured_graphic_link"];
                    gameObj.banner_url = [tempDict objectForKey:@"referral_deep_link"];
                    gameObj.screen_shots = [[NSMutableArray alloc] init];
                    
                    NSArray *screenshots = [tempDict objectForKey:@"screen_shots"];
                    for(int j=0; j<screenshots.count; j++) {
                        NSString *imageUrl = [screenshots objectAtIndex:j];
                        [gameObj.screen_shots addObject:imageUrl];
                    }
                    
                    [gamesToBeAdded addObject:gameObj];
                    
                }
                
                [SharedManager shareManager].games = gamesToBeAdded;
                
                [[NavigationHandler getInstance] NavigateToHomeScreen];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verification Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

-(void) sendForgotCall :(NSString*) strToBeSend{
    [CustomLoading showAlertMessage];
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_FORGOT forKey:@"method"];
    [postParams setObject:strToBeSend forKey:@"email"];
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            [self.view endEditing:YES];
            if(flag == 1) {
                
                //Password Sent to your email
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Sent Successfully" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
                
                _forgotPswdView.hidden = true;
                
                
            }
            else if(flag == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verification Error" message:[result objectForKey:@"error_message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

- (BOOL) isSignUpCallValidate {
    
    if(_signUpEmail.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (![self validateEmail:_signUpEmail.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email is not in valid format" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpFName.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"First Name cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpLName.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Last Name cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpUName.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Username cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpPswd.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Password cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    
    
    return true;
}

-(BOOL) isLoginCallValid {
    if(_logInEmail.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (![self validateEmail:_logInEmail.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email is not in valid format" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_logInPswd.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Password cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    return true;
}

-(BOOL)validateEmail:(NSString *)candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
@end
