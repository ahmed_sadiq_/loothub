//
//  ViewController.h
//  Funhub
//
//  Created by Hannan Khan on 11/11/15.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryPicker.h"

@interface ViewController : UIViewController <UITextFieldDelegate,CountryPickerDelegate,UIGestureRecognizerDelegate> {
    UIGestureRecognizer *tapper;
    NSString *countryCode;
}
@property (weak, nonatomic) IBOutlet UILabel *forgotPswdText;
@property (weak, nonatomic) IBOutlet UILabel *signUpTxt;
@property (weak, nonatomic) IBOutlet UILabel *SignInTxt;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *logInView;
@property (weak, nonatomic) IBOutlet UITextField *logInEmail;
@property (weak, nonatomic) IBOutlet UITextField *logInPswd;

@property (weak, nonatomic) IBOutlet UIView *signUpView;
@property (weak, nonatomic) IBOutlet UITextField *signUpEmail;
@property (weak, nonatomic) IBOutlet UITextField *signUpFName;
@property (weak, nonatomic) IBOutlet UITextField *signUpLName;
@property (weak, nonatomic) IBOutlet UITextField *signUpUName;
@property (weak, nonatomic) IBOutlet UITextField *signUpPswd;
@property (weak, nonatomic) IBOutlet UITextField *signUpPhone;
@property (weak, nonatomic) IBOutlet UITextField *signUpCountry;
@property (weak, nonatomic) IBOutlet CountryPicker *signUpCPicker;
@property (weak, nonatomic) IBOutlet UIButton *cPickerDoneBtn;
@property (strong, nonatomic) IBOutlet UIView *countrySearchView;
@property (strong, nonatomic) IBOutlet UITextField *countrySearchTxt;


@property (weak, nonatomic) IBOutlet UIView *forgotPswdView;
@property (weak, nonatomic) IBOutlet UITextField *pswdUserName;
@property (weak, nonatomic) IBOutlet UITextField *pswdUsername;

@property (weak, nonatomic) IBOutlet UITextField *remindEmailTxt;
@property (weak, nonatomic) IBOutlet UITextField *remindUserNameTxt;



- (IBAction)signInPressed:(id)sender;
- (IBAction)skipPressed:(id)sender;
- (IBAction)moveToSignUp:(id)sender;

- (IBAction)signUpPressed:(id)sender;
- (IBAction)moveToSignIn:(id)sender;
- (IBAction)signUpSkippedPressed:(id)sender;
- (IBAction)countryPressed:(id)sender;
- (IBAction)cPickerDoneBtnPressed:(id)sender;
- (IBAction)forgotPswdPressed:(id)sender;


- (IBAction)resetPswdPressed:(id)sender;
- (IBAction)remindMePressed:(id)sender;
- (IBAction)pswdSkipPressed:(id)sender;


@end

