//
//  SharedManager.m
//  Funhub
//
//  Created by Ahmed Sadiq on 23/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "SharedManager.h"

@implementation SharedManager
@synthesize user,sessionToken,games;

+(SharedManager*)shareManager{
    
    static SharedManager *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[SharedManager alloc] init];
        
    });
    return sharedInstance;
}
-(id)init
{
    if(self == [super init])
    {
        user = [[UserModel alloc] init];
        games = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}
-(void)ResetModel {
    user = [[UserModel alloc] init];
    games = [[NSMutableArray alloc] init];
}

- (void)saveCustomObject:(UserModel *)object key:(NSString *)key {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:key];
    
    [prefs synchronize];
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

- (UserModel *)loadCustomObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    UserModel *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}
@end
