//
//  tenToWinVC.h
//  Funhub
//
//  Created by Ahmed Sadiq on 19/04/2016.
//  Copyright © 2016 Hannan Khan. All rights reserved.
//
#import <FLAnimatedImage/FLAnimatedImage.h>
#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import <MessageUI/MessageUI.h>
#import <AVFoundation/AVFoundation.h>
@interface tenToWinVC : UIViewController <MFMessageComposeViewControllerDelegate,FBSDKSharingDelegate,UITableViewDataSource,UITableViewDelegate,UISearchDisplayDelegate>{
    int indexToTick;
    NSTimer *timer;
    NSTimer *timerGlowing;
    NSTimer *timerFlying;
    NSTimer *timerFinal;
    NSTimer *runTimer;
    CAKeyframeAnimation *pathAnimation;
    CGRect balanceFrame;
    
    NSDate *startTime;
    
    int miliseconds;
    int seconds;
    int minutes;
    CAShapeLayer *circle;
    CAShapeLayer *circle1;
    
    int secondsLeft;
    int totalRecords;
    int totalActiveRecords;
    int _radius;
    
    int serverTime;
    int allTicks;
    int winner;
    
    CGRect colorRingFrame;
    
    int pointsAccesingCount;
    
    BOOL isSearch;
    
    NSString *inviteURL;
    NSString *inviteMsg;
    int sharingstate; // 0 Facebook 1 Twitter 2 Instagram
    
    AVAudioPlayer *_audioPlayer;
    
    ACAccount *myAccount;
    NSMutableString *paramString;
    NSMutableArray *resultFollowersNameList;
    MFMessageComposeViewController *controller;
    
    
    int uiImageTransitionState;
    int rotateImageTransitionState;
    int rotateImgDegree;
}
@property (strong, nonatomic) IBOutlet UIImageView *rocketIcon;
@property (strong, nonatomic) IBOutlet UIImageView *archPathImage;
@property (strong, nonatomic) IBOutlet UIImageView *powerUpImg;
@property (strong, nonatomic) IBOutlet UIView *blackOverlay;
@property (strong, nonatomic) IBOutlet UIImageView *wheelImg;

@property (nonatomic, strong) FLAnimatedImageView *imageView1;
@property (nonatomic, strong) FLAnimatedImageView *imageView2;

@property (strong, nonatomic) IBOutlet UIImageView *mainBg;
@property (weak, nonatomic) IBOutlet UIButton *sendInviteBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeLeftLbl;
@property (strong, nonatomic) IBOutlet UIView *flyingCatcher;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *idLbl;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *balanceAmountLbl;
@property (weak, nonatomic) IBOutlet UIView *mailLayer;
@property (weak, nonatomic) IBOutlet UIView *layerToSlide;
@property (strong, nonatomic) IBOutlet UIImageView *glowingImg;
@property (strong, nonatomic) IBOutlet UIImageView *timePointer;
@property (strong, nonatomic) IBOutlet UIView *middleView;
@property (weak, nonatomic) IBOutlet UIImageView *timeOutline;
@property (weak, nonatomic) IBOutlet UILabel *timerLbl;
@property (weak, nonatomic) IBOutlet UILabel *invitationLbl;
@property (strong, nonatomic) IBOutlet UIImageView *colorRing;
@property (weak, nonatomic) IBOutlet UIView *inviteView;
@property (weak, nonatomic) IBOutlet UIView *inviteInnerView;
@property (weak, nonatomic) IBOutlet UILabel *inviteTxt;

@property (strong, nonatomic) UIImage *lastImageInAnimation;
@property (strong, nonatomic) NSArray *accountsList;
@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) NSMutableArray *usersText;
@property (strong, nonatomic) NSMutableArray *searchResult;
@property (strong, nonatomic) NSMutableArray *recievedInvitationsList;

@property(nonatomic,retain) ACAccount *myAccount;
@property(nonatomic, retain) NSMutableString *paramString;
@property(nonatomic, retain) NSMutableArray *resultFollowersNameList;
@property (weak, nonatomic) IBOutlet UIView *inviteListView;
@property (weak, nonatomic) IBOutlet UILabel *liveInviteLbl;
@property (weak, nonatomic) IBOutlet UITableView *inviteListTblView;
@property (weak, nonatomic) IBOutlet UIImageView *curlArrow;
@property (weak, nonatomic) UITableView *searchTblView;


@property (weak, nonatomic) IBOutlet UIView *invitationMainView;

@property BOOL isPowerUp;
@property BOOL isFullRocket;
@property BOOL is1000Gems;
@property int premiumMembers;
@property int daysPassed;


- (IBAction)drawerPressed:(id)sender;

- (IBAction)viaFb:(id)sender;
- (IBAction)viaTwiter:(id)sender;
- (IBAction)viaIg:(id)sender;
- (IBAction)viaText:(id)sender;
- (IBAction)InviteCancelPressed:(id)sender;
- (IBAction)sendInvitePressed:(id)sender;

- (IBAction)inviteBackPressed:(id)sender;
@end
