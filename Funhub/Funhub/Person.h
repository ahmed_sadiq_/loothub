//
//  Person.h
//  Maggie
//
//  Created by Ahmed Sadiq on 16/12/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *middleName;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *homeEmail;
@property (nonatomic, strong) NSString *workEmail;
@property (nonatomic, strong) NSString *number;
@property BOOL isSelected;
@property (strong, nonatomic, readonly) NSString *sectionName;
@end
