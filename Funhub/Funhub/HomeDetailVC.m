//
//  HomeDetailVC.m
//  Funhub
//
//  Created by Ahmed Sadiq on 27/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "HomeDetailVC.h"
#import "AsyncImageView.h"
#import "CustomLoading.h"
#import "SharedManager.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>

@interface HomeDetailVC ()

@end

@implementation HomeDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    [self updateModel];
    _carousel.type = iCarouselTypeCoverFlow2;
    
//    _banner.imageURL = [NSURL URLWithString:_gameModel.game_icon];
//    NSURL *url = [NSURL URLWithString:_gameModel.game_icon];
//    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    
    [_banner sd_setImageWithURL:[NSURL URLWithString:_gameModel.game_icon]
                    placeholderImage:[UIImage imageNamed:@"loadPhoto.jpg"]];
    
    //[self getDeepLinkUrl];
}

-(void) getDeepLinkUrl {
    [CustomLoading showAlertMessage];
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
    [postParams setObject:_gameModel.gln_game_id forKey:@"gln_game_id"];
    
    NSData *postData = [self encodeDictionary:postParams];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                deepLinkUrl = [result objectForKey:@"deep_link"];
                
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

-(void) onTimer {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    // Updates the variable h, adding 100 (put your own value here!)
    h += screenWidth;
    if(h>=(screenWidth*4)) {
        h = 0;
        _upperScrollView.contentOffset = CGPointMake(h, 0);
    }
    else {
        [UIView animateKeyframesWithDuration:1.0 delay:0.0 options:nil animations:^{
            [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
                _upperScrollView.contentOffset = CGPointMake(h, 0);
            }];
        } completion:^(BOOL finished) {
            //Completion Block
        }];
    }
    int pageNum = (h/screenWidth);
    _pageController.currentPage = pageNum;
}


- (void) updateModel {
    
    _gameLbl.text = _gameModel.game_name;
    _detailTxt.text = _gameModel.gameDesc;
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)mainBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)screenShotLeftBtnPress:(id)sender {
}

- (IBAction)screenShotRightBtnPress:(id)sender {
}

- (IBAction)cancelFullScreenPressed:(id)sender {
    _fullScreenView.hidden = true;
}
- (void)playVideo:(id)sender {
    NSString *strVideoURL = _gameModel.game_video_link;
    NSURL *videoURL = [NSURL URLWithString:strVideoURL] ;
    
    _player = [[MPMoviePlayerController alloc] initWithContentURL: videoURL];
    [_player prepareToPlay];
    [_player.view setFrame: self.view.frame];  // player's frame must match parent's
    [self.view addSubview: _player.view];
    // ...
    [_player play];
    
    // Remove the movie player view controller from the "playback did finish" notification observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_player];
    
    // Register this class as an observer instead
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:_player];
}
- (void)showImage:(id)sender {
    _fullScreenView.hidden = false;
    
    UIButton *senderBtn = (UIButton*)sender;
    int tag = [[sender accessibilityHint] intValue];
    
    NSDictionary *imageURLDict = [_gameModel.screen_shots objectAtIndex:tag];
    NSString *imageURL = [imageURLDict objectForKey:@"screen_shot_link"];
    
    _fullScreenImg.imageURL = [NSURL URLWithString:imageURL];
    NSURL *url = [NSURL URLWithString:imageURL];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    [_player stop];
    [_player.view removeFromSuperview];
}


- (IBAction)fbShareBtnPresssed:(id)sender {
    
    if(_gameModel.generated_url.length > 2) {
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub.",[SharedManager shareManager].user.first_name];
            
            [controller setInitialText:strToShare];
            [controller addURL:[NSURL URLWithString:_gameModel.generated_url]];
            [controller addImage:_banner.image];
            
            [self presentViewController:controller animated:YES completion:Nil];
            
            SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                if (result == SLComposeViewControllerResultCancelled) {
                    
                    NSLog(@"delete");
                    
                } else
                    
                {
                    NSLog(@"post");
                }
            };
            controller.completionHandler =myBlock;
            
            
        }else {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Account Configuration Error"
                                                  message:@"Please configure Facebook Account in Settings. "
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.first_name forKey:@"user_name"];
        [postParams setObject:_gameModel.game_id forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    _gameModel.generated_url = [result objectForKey:@"deep_link"];
                    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                        
                        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. Click the following link. \n%@",[SharedManager shareManager].user.first_name, _gameModel.generated_url];
                        
                        [controller setInitialText:strToShare];
                        [controller addURL:[NSURL URLWithString:_gameModel.generated_url]];
                        [controller addImage:[UIImage imageNamed:@"icon_1024.png"]];
                        
                        [self presentViewController:controller animated:YES completion:Nil];
                        
                        
                        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                            if (result == SLComposeViewControllerResultCancelled) {
                                
                                NSLog(@"delete");
                                
                            } else
                                
                            {
                                NSLog(@"post");
                            }
                            
                            //   [composeController dismissViewControllerAnimated:YES completion:Nil];
                        };
                        controller.completionHandler =myBlock;
                        
                        
                    }else {
                        UIAlertController *alertController = [UIAlertController
                                                              alertControllerWithTitle:@"Account Configuration Error"
                                                              message:@"Please configure Facebook Account in Settings. "
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       NSLog(@"OK action");
                                                   }];
                        
                        [alertController addAction:okAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    
    
    
}
- (IBAction)twitterShareBtnPressed:(id)sender {
    if(_gameModel.generated_url.length > 2) {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub.\n%@",[SharedManager shareManager].user.first_name, _gameModel.generated_url];
            
            [tweetSheet setInitialText:strToShare];
            [tweetSheet addImage:_banner.image];
            [tweetSheet addURL: [NSURL URLWithString:
                                 _gameModel.generated_url]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
            
            SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                if (result == SLComposeViewControllerResultCancelled) {
                    NSLog(@"delete");
                } else
                {
                    NSLog(@"post");
                }
                //   [composeController dismissViewControllerAnimated:YES completion:Nil];
            };
            tweetSheet.completionHandler =myBlock;
        }
        else {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Account Configuration Error"
                                                  message:@"Please configure Twitter Account in Settings. "
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else{
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
        [postParams setObject:_gameModel.game_id forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    _gameModel.generated_url = [result objectForKey:@"deep_link"];
                    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
                    {
                        SLComposeViewController *tweetSheet = [SLComposeViewController
                                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
                        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub.\n%@",[SharedManager shareManager].user.first_name, _gameModel.generated_url];
                        
                        [tweetSheet setInitialText:strToShare];
                        [tweetSheet addImage:[UIImage imageNamed:@"icon_1024.png"]];
                        [tweetSheet addURL: [NSURL URLWithString:
                                             _gameModel.generated_url]];
                        [self presentViewController:tweetSheet animated:YES completion:nil];
                        
                        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                            if (result == SLComposeViewControllerResultCancelled) {
                                NSLog(@"delete");
                            } else
                            {
                                NSLog(@"post");
                            }
                            
                            //   [composeController dismissViewControllerAnimated:YES completion:Nil];
                        };
                        tweetSheet.completionHandler =myBlock;
                    }
                    else {
                        UIAlertController *alertController = [UIAlertController
                                                              alertControllerWithTitle:@"Account Configuration Error"
                                                              message:@"Please configure Twitter Account in Settings. "
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       NSLog(@"OK action");
                                                   }];
                        
                        [alertController addAction:okAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
}
- (IBAction)mailBtnPressed:(id)sender {
    
    if(_gameModel.generated_url.length > 2) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to %@. ",[SharedManager shareManager].user.first_name,_gameModel.game_name];
        [controller setSubject:strToShare];
        //[Loot Hub]
        NSString *strBody = [NSString stringWithFormat:@"Hey, join the revolutionary gaming network where you will get paid for playing games! \n%@",_gameModel.generated_url];
        
        [controller setMessageBody:strBody isHTML:NO];
        if (controller) {
            [self presentViewController:controller animated:YES completion:NULL];
        }
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
        [postParams setObject:_gameModel.game_id forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    _gameModel.generated_url = [result objectForKey:@"deep_link"];
                    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                    controller.mailComposeDelegate = self;
                    NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. ",[SharedManager shareManager].user.first_name];
                    [controller setSubject:strToShare];
                    //[Loot Hub]
                    NSString *strBody = [NSString stringWithFormat:@"Hey, join the revolutionary gaming network where you will get paid for playing games! \n%@",_gameModel.generated_url];
                    
                    [controller setMessageBody:strBody isHTML:NO];
                    if (controller) {
                        [self presentViewController:controller animated:YES completion:NULL];
                    }
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
}
- (IBAction)installBtnPressed:(id)sender {
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:_gameModel.deep_link]];
    
    Game *gameObj = _gameModel;
    
    if(gameObj.install_url.length < 2 && gameObj.banner_url.length > 2) {
        gameObj.install_url = gameObj.banner_url;
    }
    
    if(gameObj.install_url.length > 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:gameObj.install_url]];
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.referrer_user_name forKey:@"user_name"];
        [postParams setObject:gameObj.gln_game_id forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    gameObj.install_url = [result objectForKey:@"deep_link"];
                    gameObj.banner_url = [result objectForKey:@"deep_link"];
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:gameObj.install_url]];
                    
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    int count =  [_gameModel.screen_shots count];
    return  count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    //create new view if no view is available for recycling
    
    
    if (view == nil)
    {
        if(index == 0 && _gameModel.game_video_link.length > 0) {
            
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
            
            CGFloat borderWidth = 3.0f;
            view.layer.borderColor = [UIColor darkGrayColor].CGColor;
            view.layer.borderWidth = borderWidth;
            
            view.layer.masksToBounds = NO;
            view.layer.shadowOffset = CGSizeMake(-15, 20);
            view.layer.shadowRadius = 4;
            view.layer.shadowOpacity = 0.1;
            
            AsyncImageView *thumbnail = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
            
            thumbnail.imageURL = [NSURL URLWithString:_gameModel.video_thumbnail];
            NSURL *url = [NSURL URLWithString:_gameModel.video_thumbnail];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            thumbnail.tag = 2;
            [view addSubview:thumbnail];
            
            NSString *strVideoURL = _gameModel.game_video_link;
            NSURL *videoURL = [NSURL URLWithString:strVideoURL] ;
            _player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
            _player.shouldAutoplay = NO;
            dispatch_queue_t imageQueue = dispatch_queue_create("Image Queue",NULL);
            dispatch_async(imageQueue, ^{
                
                
            });
            
            UIButton *thumbnailBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
            [view addSubview:thumbnailBtn];
            thumbnailBtn.tag = 3;
            
            [thumbnailBtn addTarget:self
                             action:@selector(playVideo:)
                   forControlEvents:UIControlEventTouchUpInside];
            
            [thumbnailBtn setImage:[UIImage imageNamed:@"video_play_icon.png"] forState:UIControlStateNormal];
        }
        else {
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
            
            CGFloat borderWidth = 3.0f;
            view.layer.borderColor = [UIColor darkGrayColor].CGColor;
            view.layer.borderWidth = borderWidth;
            
            view.layer.masksToBounds = NO;
            view.layer.shadowOffset = CGSizeMake(-15, 20);
            view.layer.shadowRadius = 4;
            view.layer.shadowOpacity = 0.1;
            
            AsyncImageView *thumbnail = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
            
            NSDictionary *imageURLDict = [_gameModel.screen_shots objectAtIndex:index];
            
            NSString *imageURL = [imageURLDict objectForKey:@"screen_shot_link"];
            
            thumbnail.imageURL = [NSURL URLWithString:imageURL];
            NSURL *url = [NSURL URLWithString:imageURL];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            thumbnail.tag = 1;
            
            [view addSubview:thumbnail];
            
            UIButton *thumbnailBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
            [view addSubview:thumbnailBtn];
            thumbnailBtn.tag = 4;
            thumbnailBtn.accessibilityHint = [NSString stringWithFormat:@"%i",index];
            [thumbnailBtn addTarget:self
                             action:@selector(showImage:)
                   forControlEvents:UIControlEventTouchUpInside];
            
        }
    }
    else
    {
        if(index == 0 && _gameModel.game_video_link.length > 0)
        {
            AsyncImageView *thumbnail = [view viewWithTag:2];
            
            thumbnail.imageURL = [NSURL URLWithString:_gameModel.video_thumbnail];
            NSURL *url = [NSURL URLWithString:_gameModel.video_thumbnail];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            NSString *strVideoURL = _gameModel.game_video_link;
            NSURL *videoURL = [NSURL URLWithString:strVideoURL] ;
            _player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
            _player.shouldAutoplay = NO;
            dispatch_queue_t imageQueue = dispatch_queue_create("Image Queue",NULL);
            dispatch_async(imageQueue, ^{
                
                
            });
            UIButton *thumbnailBtn = [view viewWithTag:3];
            thumbnailBtn.tag = 3;
            [thumbnailBtn addTarget:self
                             action:@selector(playVideo:)
                   forControlEvents:UIControlEventTouchUpInside];
            
            //[thumbnailBtn setImage:[UIImage imageNamed:@"video_play_icon.png"] forState:UIControlStateNormal];
            
        }
        else {
            AsyncImageView *thumbnail = [view viewWithTag:1];
            //get a reference to the label in the recycled view
            NSDictionary *imageURLDict = [_gameModel.screen_shots objectAtIndex:index];
            
            NSString *imageURL = [imageURLDict objectForKey:@"screen_shot_link"];
            
            thumbnail.imageURL = [NSURL URLWithString:imageURL];
            NSURL *url = [NSURL URLWithString:imageURL];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            UIButton *thumbnailBtn = [view viewWithTag:4];
            thumbnailBtn.tag = 4;
            thumbnailBtn.accessibilityHint = [NSString stringWithFormat:@"%i",index];
            [thumbnailBtn addTarget:self
                             action:@selector(showImage:)
                   forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 2.1;
    }
    return value;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}
@end
