//
//  ProfileVC.m
//  Funhub
//
//  Created by Ahmed Sadiq on 30/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "ProfileVC.h"
#import "DrawerVC.h"
#import "SharedManager.h"
#import "NavigationHandler.h"
#import <Pushwoosh/PushNotificationManager.h>
@interface ProfileVC ()

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    _userName.text = [NSString stringWithFormat:@"USER NAME : %@",[SharedManager shareManager].user.user_name];
    _userID.text = [NSString stringWithFormat:@"ID : %@",[SharedManager shareManager].user.imatrix_id] ;
    _balanceLbl.text = [SharedManager shareManager].user.balance;
    _referalTxt.text = [SharedManager shareManager].user.referrer_user_name;
    
    _phoneTxt.text = [SharedManager shareManager].user.phone;
    _countryTxt.text = [SharedManager shareManager].user.country;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        [_tenToWinBtn setImage:[UIImage imageNamed:@"10towin_icon.png"] forState:UIControlStateNormal];
    }
    else {
        [_tenToWinBtn setImage:[UIImage imageNamed:@"10towin_icon_active.png"] forState:UIControlStateNormal];
    }
    
    BOOL isNotificationOff = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isNotificationOff"] boolValue];
    
    if(isNotificationOff) {
        [_notificationSwitch setOn:false];
    }
    else {
        [_notificationSwitch setOn:true];
    }
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)notificationSwitchPressed:(id)sender {
    
    BOOL isNotificationOff = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isNotificationOff"] boolValue];
    
    if(isNotificationOff) {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isNotificationOff"];
        [[PushNotificationManager pushManager] registerForPushNotifications];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isNotificationOff"];
        [[PushNotificationManager pushManager] unregisterForPushNotifications];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)sliderBtnPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)editProfilePressed:(id)sender {
    if(_editBtn.tag == 0) {
        _editBtn.tag = 1;
        //Make fields available to Edit
        [_editBtn setTitle:@"Save" forState:UIControlStateNormal];
        
        _phoneTxt.enabled = true;
        _countryBtn.enabled = true;
        
    }
    else {
        _editBtn.tag = 0;
        //Save all fields
        [_editBtn setTitle:@"Edit Profile" forState:UIControlStateNormal];
        
        [[NSUserDefaults standardUserDefaults] setObject:_phoneTxt.text forKey:@"phone"];
        [[NSUserDefaults standardUserDefaults] setObject:_countryTxt.text forKey:@"country"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _phoneTxt.enabled = false;
        _countryBtn.enabled = false;
    }
}

- (IBAction)changePswdPressed:(id)sender {
}

- (IBAction)tenToWinPressed:(id)sender {
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature and earn 20 tokens every day. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToTenToWin];
    }
}

- (IBAction)countryPressed:(id)sender {
    _signUpCPicker.hidden = false;
    _cPickerDoneBtn.hidden = false;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (IBAction)cPickerDoneBtnPressed:(id)sender {
    _signUpCPicker.hidden = true;
    _cPickerDoneBtn.hidden = true;
}

#pragma mark - Country Picket Delegate Methods
- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _countryTxt.text = name;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex              {
    if(buttonIndex == 1)//Link button pressed.
    {
        NSString *username = [SharedManager shareManager].user.referrer_user_name;
        
        NSString *urlToHit = [NSString stringWithFormat:@"http://%@.playgamesgetpaid.com/CP1/",username];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToHit]];

    }
}
@end
