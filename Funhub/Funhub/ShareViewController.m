//
//  ShareViewController.m
//  Funhub
//
//  Created by Ahmed Sadiq on 29/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "ShareViewController.h"
#import "DrawerVC.h"
#import "SharedManager.h"
#import "Constants.h"
#import "CustomLoading.h"
#import <Social/Social.h>

@interface ShareViewController ()

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sliderBtnPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)shareOnFb:(id)sender {
    if(deepLinkUrl.length > 4 ) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. ",[SharedManager shareManager].user.first_name];
            
            [controller setInitialText:strToShare];
            [controller addURL:[NSURL URLWithString:deepLinkUrl]];
            [controller addImage:[UIImage imageNamed:@"icon_1024.png"]];
            
            [self presentViewController:controller animated:YES completion:Nil];
            
            
            SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            };
            controller.completionHandler =myBlock;
            
            
        }else {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Account Configuration Error"
                                                  message:@"Please configure Facebook Account in Settings. "
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
        [postParams setObject:@"5" forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    deepLinkUrl = [result objectForKey:@"deep_link"];
                    
                    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                        
                        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. ",[SharedManager shareManager].user.first_name];
                        
                        [controller setInitialText:strToShare];
                        [controller addURL:[NSURL URLWithString:deepLinkUrl]];
                        [controller addImage:[UIImage imageNamed:@"icon_1024.png"]];
                        
                        [self presentViewController:controller animated:YES completion:Nil];
                        
                        
                        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        };
                        controller.completionHandler =myBlock;
                        
                        
                    }else {
                        UIAlertController *alertController = [UIAlertController
                                                              alertControllerWithTitle:@"Account Configuration Error"
                                                              message:@"Please configure Facebook Account in Settings. "
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       NSLog(@"OK action");
                                                   }];
                        
                        [alertController addAction:okAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    
}

- (IBAction)shareOnTwitter:(id)sender {
    if(deepLinkUrl.length > 4) {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. %@",[SharedManager shareManager].user.first_name,deepLinkUrl];
            [tweetSheet setInitialText:strToShare];
            [tweetSheet addImage:[UIImage imageNamed:@"icon_1024.png"]];
//            [tweetSheet addURL: [NSURL URLWithString:
//                                 deepLinkUrl]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
            
            SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            };
            tweetSheet.completionHandler =myBlock;
        }
        else {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Account Configuration Error"
                                                  message:@"Please configure Twitter Account in Settings. "
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
        [postParams setObject:@"5" forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    deepLinkUrl = [result objectForKey:@"deep_link"];
                    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
                    {
                        SLComposeViewController *tweetSheet = [SLComposeViewController
                                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
                        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. %@",[SharedManager shareManager].user.first_name,deepLinkUrl];
                        [tweetSheet setInitialText:strToShare];
                        [tweetSheet addImage:[UIImage imageNamed:@"icon_1024.png"]];
//                        [tweetSheet addURL: [NSURL URLWithString:
//                                             deepLinkUrl]];
                        [self presentViewController:tweetSheet animated:YES completion:nil];
                        
                        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                        };
                        tweetSheet.completionHandler =myBlock;
                    }
                    else {
                        UIAlertController *alertController = [UIAlertController
                                                              alertControllerWithTitle:@"Account Configuration Error"
                                                              message:@"Please configure Twitter Account in Settings. "
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       NSLog(@"OK action");
                                                   }];
                        [alertController addAction:okAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    
}

- (IBAction)shareOnEmail:(id)sender {
    
    if(deepLinkUrl.length > 4) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        
        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. ",[SharedManager shareManager].user.first_name];
        
        [controller setSubject:strToShare];
        if(_emailTxt.text.length>0) {
            [controller setToRecipients:[NSArray arrayWithObjects:_emailTxt.text,nil]];
        }
        
        NSString *strBody = [NSString stringWithFormat:@"Hey, join the revolutionary gaming network where you will get paid for playing games! \n%@",deepLinkUrl];
        
        [controller setMessageBody:strBody isHTML:NO];
        if (controller) {
            [self presentViewController:controller animated:YES completion:NULL];
        }
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
        [postParams setObject:@"5" forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    deepLinkUrl = [result objectForKey:@"deep_link"];
                    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                    controller.mailComposeDelegate = self;
                    
                    NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub. ",[SharedManager shareManager].user.first_name];
                    
                    [controller setSubject:strToShare];
                    if(_emailTxt.text.length>0) {
                        [controller setToRecipients:[NSArray arrayWithObjects:_emailTxt.text,nil]];
                    }
                    
                    NSString *strBody = [NSString stringWithFormat:@"Hey, join the revolutionary gaming network where you will get paid for playing games! \n%@",deepLinkUrl];
                    
                    [controller setMessageBody:strBody isHTML:NO];
                    if (controller) {
                        [self presentViewController:controller animated:YES completion:NULL];
                    }
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    
    
}

- (IBAction)sendBtnPressed:(id)sender {
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    else {
        
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}
@end
