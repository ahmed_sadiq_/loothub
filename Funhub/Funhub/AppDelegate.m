//
//  AppDelegate.m
//  Funhub
//
//  Created by Hannan Khan on 11/11/15.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "NavigationHandler.h"
#import "HomeVC.h"
#import "Branch.h"
#import "HelpshiftSupport.h"
#import "HelpshiftCore.h"
#import "CustomLoading.h"
#import "SharedManager.h"
#import "Constants.h"
#import "UserModel.h"
#import "Flurry.h"

#import <CoreLocation/CoreLocation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import <Pushwoosh/PushNotificationManager.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize viewController, navigationController,referralId,tokensPurchased;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    NavigationHandler *navHandler = [[NavigationHandler alloc] initWithMainWindow:self.window];
    
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error) {
            // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
            // params will be empty if no data found
            // ... insert custom logic here ...
            NSLog(@"params: %@", params.description);
            NSString *referId = [params objectForKey:@"referral"];
            
            NSRange range = [referId rangeOfString:@"_"];
            referId = [referId substringToIndex:range.location];
            
            if(referId.length > 1) {
                referralId = [params objectForKey:@"referral"];
                [[NSUserDefaults standardUserDefaults] setObject:referId forKey:@"referrer_imatrix_id"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            
        }
    }];
    
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
    
    if(isLoggedIn) {
        [navHandler NavigateToHomeScreen];
    }
    else {
        [navHandler loadFirstVC];
    }
    [self.window makeKeyAndVisible];
    
    [HelpshiftCore initializeWithProvider:[HelpshiftSupport sharedInstance]];
    [HelpshiftCore installForApiKey:@"4c8504ff2c25c26d9bac25f9a6dd105d" domainName:@"gamelootnetwork.helpshift.com" appID:@"gamelootnetwork_platform_20151203101917268-af36fee1f13bec4"];
    
    //-----------PUSHWOOSH PART-----------
    // set custom delegate for push handling, in our case - view controller
    PushNotificationManager * pushManager = [PushNotificationManager pushManager];
    pushManager.delegate = self;
    
    // handling push on app start
    [[PushNotificationManager pushManager] handlePushReceived:launchOptions];
    
    // make sure we count app open in Pushwoosh stats
    [[PushNotificationManager pushManager] sendAppOpen];
    
    [SharedManager shareManager].user = [[SharedManager shareManager] loadCustomObjectWithKey:@"userTempModel"];
    
    NSString *imatrixID = [SharedManager shareManager].user.imatrix_id;
    
    if(imatrixID) {
        NSDictionary *tags = [NSDictionary dictionaryWithObjectsAndKeys:
                              imatrixID, @"imatrix_id",
                              nil];
        [[PushNotificationManager pushManager] setTags:tags];
    }
    
    BOOL isNotificationOff = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isNotificationOff"] boolValue];
    
    if(isNotificationOff) {
        // register for push notifications!
        [[PushNotificationManager pushManager] unregisterForPushNotifications];
    }
    else {
        // register for push notifications!
        [[PushNotificationManager pushManager] registerForPushNotifications];
    }
    
    [Fabric with:@[[Crashlytics class]]];
    
    [Flurry startSession:@"T52CY4DM5QB5XD7MFBNH"];
    
    return YES;
}

// system push notification registration success callback, delegate to pushManager
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
}

// system push notification registration error callback, delegate to pushManager
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[PushNotificationManager pushManager] handlePushRegistrationFailure:error];
}

// system push notifications callback, delegate to pushManager
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[PushNotificationManager pushManager] handlePushReceived:userInfo];
}

- (void) onPushAccepted:(PushNotificationManager *)pushManager withNotification:(NSDictionary *)pushNotification {
    NSLog(@"Push notification received");
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)recieveInventoryUpdate:(NSNotification *)notification {
    
    int tokens = [[SharedManager shareManager].user.balance intValue];
    tokens = tokens+tokensPurchased;
    
    NSString *totalTokens = [NSString stringWithFormat:@"%d",tokens];
    
    UserModel *uModel = [SharedManager shareManager].user;
    uModel.balance = totalTokens;
    
    [SharedManager shareManager].user = uModel;
    
    [[SharedManager shareManager] saveCustomObject:uModel key:@"userTempModel"];
    
    [[NSUserDefaults standardUserDefaults] setObject:totalTokens forKey:@"imatrix_total_tokens"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_UPDATE_TOKENS forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"imatrix_id"];
    [postParams setObject:totalTokens forKey:@"no_of_tokens"];
    [postParams setObject:@"add" forKey:@"action_type"];
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                NSLog(@"Success");
            }
            else {
                NSLog(@"Failed");
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}
- (void)recieveInventoryUpdateFailure:(NSNotification *)notification {
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // pass the url to the handle deep link call
    [[Branch getInstance] handleDeepLink:url];
    
    // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    return YES;
}
-(NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window{
    return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate{
    return NO;
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}
@end
