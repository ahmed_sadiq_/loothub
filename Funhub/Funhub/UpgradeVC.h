//
//  UpgradeVC.h
//  Funhub
//
//  Created by Ahmed Sadiq on 29/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface UpgradeVC : UIViewController <NIDropDownDelegate,UITextFieldDelegate> {
    NIDropDown *dropDown;
    UIGestureRecognizer *tapper;
    NSString *typeCurrent;
    BOOL isPremium;
}

- (IBAction)sliderBtnPressed:(id)sender;
- (IBAction)upgradePremiumUser:(id)sender;
- (IBAction)upgradeAmbassadorUser:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *billingView;
@property (weak, nonatomic) IBOutlet UITextField *cardTypeTxt;
@property (weak, nonatomic) IBOutlet UITextField *holderNAme;
@property (weak, nonatomic) IBOutlet UITextField *cardNumTxt;
@property (weak, nonatomic) IBOutlet UITextField *cvcDigitTxt;
@property (weak, nonatomic) IBOutlet UIButton *cardTypetn;
@property (weak, nonatomic) IBOutlet UIButton *expiryMonthBtn;
@property (weak, nonatomic) IBOutlet UIButton *expiryYearBtn;
@property (weak, nonatomic) IBOutlet UIButton *countryBtn;
@property (weak, nonatomic) IBOutlet UITextField *streetAddressTxt;
@property (weak, nonatomic) IBOutlet UITextField *streetAddressTxt2;
@property (weak, nonatomic) IBOutlet UITextField *cityTxt;
@property (weak, nonatomic) IBOutlet UITextField *zipTxt;
@property (weak, nonatomic) IBOutlet UIView *formView;
@property (weak, nonatomic) IBOutlet UIImageView *popUpImg;

@property (weak, nonatomic) IBOutlet UIButton *premiumBtn;
@property (weak, nonatomic) IBOutlet UIButton *ambassodorBtn;
@property (weak, nonatomic) IBOutlet UIImageView *premiumBg;
@property (weak, nonatomic) IBOutlet UIImageView *ambassororBg;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)billingViewClosePressed:(id)sender;
- (IBAction)cardTypePressed:(id)sender;
- (IBAction)expiryMonthPressed:(id)sender;
- (IBAction)expiryYearPressed:(id)sender;
- (IBAction)countryPressed:(id)sender;
- (IBAction)billingSubmitPressed:(id)sender;


- (IBAction)nextPressed:(id)sender;
- (IBAction)premimumPressed:(id)sender;
- (IBAction)ambasodorPressed:(id)sender;
@end
