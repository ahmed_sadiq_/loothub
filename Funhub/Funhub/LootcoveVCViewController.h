//
//  LootcoveVCViewController.h
//  Funhub
//
//  Created by Ahmed Sadiq on 29/04/2016.
//  Copyright © 2016 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LootcoveVCViewController : UIViewController<UIWebViewDelegate>
- (IBAction)sliderPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *tenToWinBtn;
- (IBAction)tenToWinPressed:(id)sender;

@end
