//
//  DEMOLeftMenuViewController.h
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "AsyncImageView.h"
@interface DEMOLeftMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate,UIAlertViewDelegate> {
    __weak IBOutlet UIImageView *profileImg;
    __weak IBOutlet UILabel *usernameLbl;
    __weak IBOutlet UILabel *usernameIdLbl;
    __weak IBOutlet UILabel *userBalLbl;
    
    
    __weak IBOutlet UIImageView *dimmer;
}
@property int tag;
@property (strong, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

-(IBAction)generalAction:(id)sender;
- (IBAction)logTicketPressed:(id)sender;
- (IBAction)appSettngs:(id)sender;

- (IBAction)homePressed:(id)sender;

- (IBAction)logOutPressed:(id)sender;
- (IBAction)towinPressed:(id)sender;
- (IBAction)powerUpPressed:(id)sender;

- (IBAction)shopPressed:(id)sender;
- (IBAction)sliderPressed:(id)sender;
- (IBAction)lootCovePressed:(id)sender;

@end
