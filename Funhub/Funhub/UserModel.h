//
//  UserModel.h
//  Funhub
//
//  Created by Ahmed Sadiq on 23/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *created_date;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *first_name;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *imatrix_id;
@property (strong, nonatomic) NSString *last_name;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *referrer_id;
@property (strong, nonatomic) NSString *user_name;
@property (strong, nonatomic) NSString *user_type;
@property (strong, nonatomic) NSString *balance;
@property (strong, nonatomic) NSString *referrer_user_name;

@end
