//
//  ProfileVC.h
//  Funhub
//
//  Created by Ahmed Sadiq on 30/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryPicker.h"

@interface ProfileVC : UIViewController<CountryPickerDelegate,UIAlertViewDelegate> {
    UIGestureRecognizer *tapper;
}
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userID;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *tenToWinBtn;

@property (weak, nonatomic) IBOutlet UITextField *pswdTxt;
@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;
@property (weak, nonatomic) IBOutlet UITextField *countryTxt;
@property (weak, nonatomic) IBOutlet UITextField *referalTxt;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *countryBtn;
@property (strong, nonatomic) IBOutlet UISwitch *notificationSwitch;

@property (weak, nonatomic) IBOutlet CountryPicker *signUpCPicker;
@property (weak, nonatomic) IBOutlet UIButton *cPickerDoneBtn;

- (IBAction)notificationSwitchPressed:(id)sender;

- (IBAction)sliderBtnPressed:(id)sender;
- (IBAction)editProfilePressed:(id)sender;
- (IBAction)changePswdPressed:(id)sender;
- (IBAction)tenToWinPressed:(id)sender;

- (IBAction)countryPressed:(id)sender;
@end
