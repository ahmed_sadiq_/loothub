//
//  UpgradeScreen2.h
//  Funhub
//
//  Created by Ahmed Sadiq on 12/12/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpgradeScreen2 : UIViewController {
    int selctorPremium;
    int selectorAmbassodor;
    
    int fun_pack;
}

@property BOOL isPremium;
@property (weak, nonatomic) IBOutlet UIView *premiumView;
@property (weak, nonatomic) IBOutlet UIView *ambassodorView;
@property (weak, nonatomic) IBOutlet UIImageView *firstPack;
@property (weak, nonatomic) IBOutlet UIImageView *secondPack;
@property (weak, nonatomic) IBOutlet UIImageView *thirdPack;

@property (weak, nonatomic) IBOutlet UIImageView *firstAPack;
@property (weak, nonatomic) IBOutlet UIImageView *secondAPack;


- (IBAction)ambassodorFirstPressed:(id)sender;
- (IBAction)ambassodorSecondPressed:(id)sender;


- (IBAction)firstPackPressed:(id)sender;
- (IBAction)secondPackPressed:(id)sender;
- (IBAction)thirdPackPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)nextPressed:(id)sender;
- (IBAction)sliderPressed:(id)sender;

@end
