//
//  UpgradeScreen2.m
//  Funhub
//
//  Created by Ahmed Sadiq on 12/12/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "UpgradeScreen2.h"
#import "DrawerVC.h"
#import "SharedManager.h"
#import "NavigationHandler.h"

@interface UpgradeScreen2 ()

@end

@implementation UpgradeScreen2
@synthesize isPremium;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(isPremium) {
        _premiumView.hidden = true;
        _ambassodorView.hidden = false;
        selectorAmbassodor = -1;
        selctorPremium = 0;
        
        fun_pack = 101;
    }
    else {
        _premiumView.hidden = false;
        _ambassodorView.hidden = true;
        selectorAmbassodor = 0;
        selctorPremium = -1;
        fun_pack = 103;
    }
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ambassodorFirstPressed:(id)sender {
    selectorAmbassodor = 0;
    
    _firstAPack.image = [UIImage imageNamed:@"funpack50card_active.png"];
    _secondAPack.image = [UIImage imageNamed:@"funpack250card.png"];
    
    fun_pack = 101;
}

- (IBAction)ambassodorSecondPressed:(id)sender {
    selectorAmbassodor = 1;
    
    _firstAPack.image = [UIImage imageNamed:@"funpack50card.png"];
    _secondAPack.image = [UIImage imageNamed:@"funpack250card_active.png"];
    
    fun_pack = 102;
}

- (IBAction)firstPackPressed:(id)sender {
    selctorPremium = 0;
    
    _firstPack.image = [UIImage imageNamed:@"funpack500card_active.png"];
    _secondPack.image = [UIImage imageNamed:@"funpack1250card.png"];
    _thirdPack.image = [UIImage imageNamed:@"funpack2500card.png"];
    
    fun_pack = 103;
}

- (IBAction)secondPackPressed:(id)sender {
    selctorPremium = 1;
    _firstPack.image = [UIImage imageNamed:@"funpack500card.png"];
    _secondPack.image = [UIImage imageNamed:@"funpack1250card_active.png"];
    _thirdPack.image = [UIImage imageNamed:@"funpack2500card.png"];
    
    fun_pack = 104;
}

- (IBAction)thirdPackPressed:(id)sender {
    selctorPremium = 2;
    _firstPack.image = [UIImage imageNamed:@"funpack500card.png"];
    _secondPack.image = [UIImage imageNamed:@"funpack1250card.png"];
    _thirdPack.image = [UIImage imageNamed:@"funpack2500card_active.png"];
    
    fun_pack = 105;
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)nextPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToUpgrade3:fun_pack andPreimum:isPremium];
}

- (IBAction)sliderPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}
@end
