//
//  UpgradeScreen3.h
//  Funhub
//
//  Created by Ahmed Sadiq on 12/12/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
@interface UpgradeScreen3 : UIViewController <UITextFieldDelegate,NIDropDownDelegate> {
    BOOL isCC;
    NIDropDown *dropDown;
    UIGestureRecognizer *tapper;
    CGRect submitBtnFrame;
    NSMutableArray *countryCodes;
    NSMutableArray *countryNames;
    NSMutableArray *stateCodes;
    NSMutableArray *stateNames;
    
    NSString *selectedCountryCode;
    NSString *selectedStateCode;
    
    BOOL isCountrySelected;
    
    NSArray *countries;
}


@property int fun_pack;
@property BOOL isPremium;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroller;

@property (weak, nonatomic) IBOutlet UIButton *countryPressed;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *companyName;
@property (weak, nonatomic) IBOutlet UITextField *ssn;
@property (weak, nonatomic) IBOutlet UITextField *ssnRepeat;
@property (weak, nonatomic) IBOutlet UITextField *address1;
@property (weak, nonatomic) IBOutlet UITextField *address2;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UITextField *phoneNum;
@property (weak, nonatomic) IBOutlet UITextField *zip;
@property (weak, nonatomic) IBOutlet UILabel *selectedItem;
@property (weak, nonatomic) IBOutlet UILabel *monthlyPlan;
@property (weak, nonatomic) IBOutlet UIView *ccDetail;
@property (weak, nonatomic) IBOutlet UITextField *cardHolderName;
@property (weak, nonatomic) IBOutlet UITextField *cardNum;
@property (weak, nonatomic) IBOutlet UITextField *ccvNum;
@property (weak, nonatomic) IBOutlet UIButton *ccBtn;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *expiryBtn;
@property (weak, nonatomic) IBOutlet UIButton *ExpiryYearBtn;

@property (weak, nonatomic) IBOutlet UIButton *submitBankBtn;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;


- (IBAction)ccPressed:(id)sender;
- (IBAction)bankWirePressed:(id)sender;
- (IBAction)expiryMonthPressed:(id)sender;
- (IBAction)expiryYearPressed:(id)sender;
- (IBAction)submitPressed:(id)sender;
- (IBAction)countryListPressed:(id)sender;

- (IBAction)statePressed:(id)sender;

@end
