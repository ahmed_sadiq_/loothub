//
//  HomeDetailVC.h
//  Funhub
//
//  Created by Ahmed Sadiq on 27/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"
#import "AsyncImageView.h"
#import "iCarousel.h"
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface HomeDetailVC : UIViewController<MFMailComposeViewControllerDelegate,iCarouselDataSource,iCarouselDelegate> {
    int h;
    NSString *deepLinkUrl;
}
@property (weak, nonatomic) IBOutlet UIImageView *banner;

- (IBAction)mainBackPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *upperScrollView;
@property (weak, nonatomic) IBOutlet UILabel *gameLbl;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (weak, nonatomic) IBOutlet UIScrollView *screenShotsScroller;
@property (weak, nonatomic) IBOutlet UITextView *detailTxt;
@property (strong, nonatomic) Game *gameModel;
@property (strong, nonatomic) MPMoviePlayerController *player;
@property (weak, nonatomic) IBOutlet UIView *fullScreenView;
@property (weak, nonatomic) IBOutlet AsyncImageView *fullScreenImg;

@property (nonatomic, strong) IBOutlet iCarousel *carousel;

- (IBAction)screenShotLeftBtnPress:(id)sender;
- (IBAction)screenShotRightBtnPress:(id)sender;
- (IBAction)cancelFullScreenPressed:(id)sender;


@end
