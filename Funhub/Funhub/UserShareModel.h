//
//  UserShareModel.h
//  Funhub
//
//  Created by Ahmed Sadiq on 24/04/2016.
//  Copyright © 2016 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserShareModel : NSObject
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *userID;
@end
