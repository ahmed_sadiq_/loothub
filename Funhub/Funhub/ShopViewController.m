//
//  ShopViewController.m
//  Funhub
//
//  Created by Ahmed Sadiq on 26/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "ShopViewController.h"
#import "DrawerVC.h"
#import "InAppPurchaseManager.h"
#import "CustomLoading.h"
#import "SharedManager.h"
@interface ShopViewController ()

@end

@implementation ShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    _appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [[NSNotificationCenter defaultCenter] addObserver:_appDel selector:@selector(recieveInventoryUpdate:) name:@"kInAppPurchaseManagerTransactionSucceededNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:_appDel selector:@selector(recieveInventoryUpdateFailure:) name:@"kInAppPurchaseManagerProductsFetchedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UIUpdaterSuccess:) name:@"kInAppPurchaseManagerTransactionSucceededNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UIUpdaterFailure:) name:@"kInAppPurchaseManagerProductsFetchedNotification" object:nil];
    
    _userName.text = [NSString stringWithFormat:@"User Name : %@",[SharedManager shareManager].user.user_name];
    _UserId.text = [NSString stringWithFormat:@"ID : %@",[SharedManager shareManager].user.imatrix_id] ;
    _balanceLbl.text = [SharedManager shareManager].user.balance;
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}
// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sliderPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)buy5TokensPressed:(id)sender {
    InAppPurchaseManager *shared = [InAppPurchaseManager sharedInstance];
    [shared loadStore:0];
    if([shared canMakePurchases]){
        [CustomLoading showAlertMessage];
        _appDel.tokensPurchased = 5;
        [shared purchase5Tokens];
    }
}
- (IBAction)buy25TokensPressed:(id)sender {
    InAppPurchaseManager *shared = [InAppPurchaseManager sharedInstance];
    [shared loadStore:1];
    if([shared canMakePurchases]){
        [CustomLoading showAlertMessage];
        _appDel.tokensPurchased = 25;
        [shared purchase25Tokens];
    }
}
- (IBAction)buy50TokensPressed:(id)sender {
    InAppPurchaseManager *shared = [InAppPurchaseManager sharedInstance];
    [shared loadStore:2];
    if([shared canMakePurchases]){
        [CustomLoading showAlertMessage];
        _appDel.tokensPurchased = 50;
        [shared purchase50Tokens];
    }
}
- (IBAction)buy100TokensPressed:(id)sender {
    InAppPurchaseManager *shared = [InAppPurchaseManager sharedInstance];
    [shared loadStore:3];
    if([shared canMakePurchases]){
        [CustomLoading showAlertMessage];
        _appDel.tokensPurchased = 100;
        [shared purchase100Tokens];
    }
}
- (IBAction)buy250TokensPressed:(id)sender {
    InAppPurchaseManager *shared = [InAppPurchaseManager sharedInstance];
    [shared loadStore:4];
    if([shared canMakePurchases]){
        [CustomLoading showAlertMessage];
        _appDel.tokensPurchased = 250;
        [shared purchase250Tokens];
    }
}
- (IBAction)buy500TokensPressed:(id)sender {
    InAppPurchaseManager *shared = [InAppPurchaseManager sharedInstance];
    [shared loadStore:5];
    if([shared canMakePurchases]){
        [CustomLoading showAlertMessage];
        _appDel.tokensPurchased = 500;
        [shared purchase500Tokens];
    }
}

- (void)UIUpdaterSuccess:(NSNotification *)notification {
    
//    int points = [cashablePoints.text intValue];
//    points = points+100;
//    
//    NSString *totalPoints = [NSString stringWithFormat:@"%d",points];
//    [cashablePoints setText:totalPoints];
    _balanceLbl.text = [SharedManager shareManager].user.balance;
    
}
- (void)UIUpdaterFailure:(NSNotification *)notification {
    //[loadView hide];
}
@end
