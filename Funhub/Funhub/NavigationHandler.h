//
//  NavigationHandler.h
//  Yolo
//
//  Created by Salman Khalid on 13/06/2014.
//  Copyright (c) 2014 Xint Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Game.h"

@interface NavigationHandler : NSObject{
    
    UINavigationController *navController;
    UIWindow *_window;
    
     AppDelegate *appDelegate;
}

-(id)initWithMainWindow:(UIWindow *)_tempWindow;
-(void)loadFirstVC;

+(NavigationHandler *)getInstance;
-(void)NavigateToHomeScreen;
-(void)NavigateToLoginScreen;
-(void)MoveToShopVC;
-(void)MoveToShareVC;
-(void)MoveToUpgrades;
-(void)MoveToProfile;
-(void)MoveToTenToWin;
-(void)MoveToPowerUp;
-(void)MoveToLootCove;
-(void)MoveToUpgrade3 : (int) fun_Pack andPreimum : (BOOL)isPremium;
-(void)MoveToUpgrade2 : (BOOL) isPremium;
-(void)MoveToHomeDetail : (Game*) gameObj;
@end
