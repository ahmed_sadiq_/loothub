//
//  ShareViewController.h
//  Funhub
//
//  Created by Ahmed Sadiq on 29/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface ShareViewController : UIViewController <MFMailComposeViewControllerDelegate>  {
    UIGestureRecognizer *tapper;
    NSString *deepLinkUrl;
}

- (IBAction)sliderBtnPressed:(id)sender;
- (IBAction)shareOnFb:(id)sender;
- (IBAction)shareOnTwitter:(id)sender;
- (IBAction)shareOnEmail:(id)sender;
- (IBAction)sendBtnPressed:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@end
