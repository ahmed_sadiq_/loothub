//
//  DEMOLeftMenuViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"
#import "HomeVC.h"
#import "UIViewController+RESideMenu.h"

#import "Constants.h"
#import "CustomLoading.h"
#import "UIImageView+RoundImage.h"
#import "SharedManager.h"
#import "NavigationHandler.h"
#import "HelpshiftSupport.h"

#import "MenuCell.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
@interface DEMOLeftMenuViewController ()
{
    
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (IS_IPHONE_6) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 667);
    }else if (IS_IPHONE_6_PLUS){
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 736);
    }
    
    [profileImg roundImageCorner];
    
    NSString *profileUrlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_profile"];
    
    NSURL* url = [NSURL URLWithString:profileUrlStr];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,
                                               NSData * data,
                                               NSError * error) {
                               if (!error){
                                   profileImg.image = [UIImage imageWithData:data];
                                   [profileImg roundImageCorner];
                                   // do whatever you want with image
                               }
                               
                           }];

}

- (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}
-(void) updateUserProfile:(NSNotification *) notification{
    if ([notification.name isEqualToString:@"updateUserProfile"])
    {
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
        NSString *userImg = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Img"];
        _lblUserName.text = userName;
        [_userImg sd_setImageWithURL:[NSURL URLWithString:userImg] placeholderImage:[UIImage imageNamed:@""]];

    }
    else if ([notification.name isEqualToString:@"updateUserProfileImg"]){
        NSDictionary* userInfo = notification.object;
        UIImage *userImg = userInfo[@"IMG"];
        [_userImg setImage:userImg];
        
    }
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateUserProfile" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateUserProfileImg" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserProfile:)
                                                 name:@"updateUserProfile"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserProfile:)
                                                 name:@"updateUserProfileImg"
                                               object:nil];
    
    
    usernameLbl.text = [NSString stringWithFormat:@"USER NAME : %@",[SharedManager shareManager].user.user_name];
    usernameIdLbl.text = [NSString stringWithFormat:@"ID : %@",[SharedManager shareManager].user.imatrix_id] ;
    
    userBalLbl.text = [SharedManager shareManager].user.balance;
    
//    _userImg.imageURL = [NSURL URLWithString:userImg];
//    NSURL *url = [NSURL URLWithString:userImg];
//    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    //[_userImg sd_setImageWithURL:[NSURL URLWithString:userImg] placeholderImage:[UIImage imageNamed:@""]];

   // [_tableView reloadData];


    
}
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        return 74;
    }
    else if (IS_IPHONE_5){
        return 40;
        
    }
    else
        return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    MenuCell * cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:self options:nil];
        if(IS_IPAD) {
            cell=[nib objectAtIndex:1];
        }
        else{
            cell=[nib objectAtIndex:0];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    
    NSArray *titles = @[@"My Corner", @"My Archive", @"Trending", @"Who to follow", @"Notification",@"Beam from Gallery",@"Chat",@"Sign out"];
    NSArray *images = @[@"IconHome", @"IconCalendar", @"IconProfile", @"IconSettings", @"IconEmpty"];
    cell.lblItem.text = titles[indexPath.row];
    
    return cell;
}

- (IBAction)homePressed:(id)sender {
    [[NavigationHandler getInstance] NavigateToHomeScreen];
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)logOutPressed:(id)sender {
    
    [CustomLoading showAlertMessage];
    [self logoutUser];
}

- (IBAction)towinPressed:(id)sender {
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature and earn 20 tokens every day. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToTenToWin];
        [self.sideMenuViewController hideMenuViewController];
    }
}

- (IBAction)powerUpPressed:(id)sender {
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToPowerUp];
        [self.sideMenuViewController hideMenuViewController];
    }
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1) {
        NSString *username = [SharedManager shareManager].user.referrer_user_name;
        
        NSString *urlToHit = [NSString stringWithFormat:@"http://%@.playgamesgetpaid.com/CP1/",username];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToHit]];
    }
}

- (IBAction)shopPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToShopVC];
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)sliderPressed:(id)sender {
}

- (IBAction)lootCovePressed:(id)sender {
    [[NavigationHandler getInstance] MoveToLootCove];
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)sharePressed:(id)sender {
    [[NavigationHandler getInstance] MoveToShareVC];
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)upgradesPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToUpgrades];
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)profilePressed:(id)sender {
    [[NavigationHandler getInstance] MoveToProfile];
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)helpPressed:(id)sender {
    [HelpshiftSupport showFAQs:self withOptions:nil];
    [self.sideMenuViewController hideMenuViewController];
}

-(void) logoutUser {
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_LOGOUT forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].sessionToken forKey:@"session_token"];
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imatrix_total_tokens"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"country"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"referrerName"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"hasAgreed"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if(flag == 1) {
                
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[SharedManager shareManager] ResetModel];
                
                [[NavigationHandler getInstance] NavigateToLoginScreen];
                [self.sideMenuViewController hideMenuViewController];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[SharedManager shareManager] ResetModel];
                
                [[NavigationHandler getInstance] NavigateToLoginScreen];
                [self.sideMenuViewController hideMenuViewController];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

@end
