//
//  HomeCell.m
//  Funhub
//
//  Created by Ahmed Sadiq on 12/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
