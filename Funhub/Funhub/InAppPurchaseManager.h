//
//  InAppPurchaseManager.h
//  SleepTime
//
//  Created by Apple on 23/04/2014.
//  Copyright (c) 2014 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"

@interface InAppPurchaseManager : NSObject<SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    SKProduct *proUpgradeProduct;
    SKProductsRequest *productsRequest;
}
+ (InAppPurchaseManager *)sharedInstance;
- (void)requestProUpgradeProductData;
- (void)loadStore : (int) type;
- (BOOL)canMakePurchases;

- (void)purchase5Tokens;
- (void)purchase25Tokens;
- (void)purchase50Tokens;
- (void)purchase100Tokens;
- (void)purchase250Tokens;
- (void)purchase500Tokens;
@end
