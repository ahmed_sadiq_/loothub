//
//  UpgradeScreen3.m
//  Funhub
//
//  Created by Ahmed Sadiq on 12/12/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "UpgradeScreen3.h"
#import "DrawerVC.h"
#import "SharedManager.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "NavigationHandler.h"
#import <QuartzCore/QuartzCore.h>

@interface UpgradeScreen3 ()

@end

@implementation UpgradeScreen3
@synthesize fun_pack,isPremium;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _alertView.layer.cornerRadius = 5;
    _alertView.layer.masksToBounds = YES;
    
    _alertView.layer.borderColor = [UIColor colorWithRed:0.666 green:0.121 blue:0.164 alpha:1.0].CGColor;
    _alertView.layer.borderWidth = 1.0;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if(IS_IPAD) {
        _mainScroller.contentSize = CGSizeMake(screenWidth, 1450);
    }
    else {
        _mainScroller.contentSize = CGSizeMake(screenWidth, 1250);
    }
    
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    submitBtnFrame = _submitBtn.frame;
    
    if(isPremium) {
        switch (fun_pack) {
        case 101:
            _selectedItem.text = @"Fun Pack 50 ($10)";
            break;
        case 102:
            _selectedItem.text = @"Fun Pack 250 ($50)";
            break;
            
        default:
            break;
        }
    }
    else {
        switch (fun_pack) {
            case 103:
                _selectedItem.text = @"Fun Pack 500 ($100)";
                break;
            case 104:
                _selectedItem.text = @"Fun Pack 1250 ($250)";
                break;
            case 105:
                _selectedItem.text = @"Fun Pack 2500 ($500)";
                break;
                
            default:
                break;
        }
    }
    
    countryCodes = [[NSMutableArray alloc] init];
    countryNames = [[NSMutableArray alloc] init];
    stateCodes = [[NSMutableArray alloc] init];
    stateNames = [[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Country" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    countries = [json objectForKey:@"Countries"];
    
    
    for(int i=0; i<countries.count; i++){
        NSDictionary *tempCountry = [countries objectAtIndex:i];
        NSString *countryName = [tempCountry objectForKey:@"name"];
        NSString *countryCode = [tempCountry objectForKey:@"code"];
        
        [countryCodes addObject:countryCode];
        [countryNames addObject:countryName];
    }
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _firstName.leftView = paddingView;
    _firstName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _lastName.leftView = paddingView1;
    _lastName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _companyName.leftView = paddingView2;
    _companyName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _ssn.leftView = paddingView3;
    _ssn.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _ssnRepeat.leftView = paddingView4;
    _ssnRepeat.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _address1.leftView = paddingView5;
    _address1.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _address2.leftView = paddingView6;
    _address2.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _city.leftView = paddingView7;
    _city.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _zip.leftView = paddingView8;
    _zip.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _state.leftView = paddingView9;
    _state.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView10 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _phoneNum.leftView = paddingView10;
    _phoneNum.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView11 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _cardHolderName.leftView = paddingView11;
    _cardHolderName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView12 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _cardNum.leftView = paddingView12;
    _cardNum.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView13 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _ccvNum.leftView = paddingView13;
    _ccvNum.leftViewMode = UITextFieldViewModeAlways;
    
    _firstName.text = [SharedManager shareManager].user.first_name;
    _lastName.text = [SharedManager shareManager].user.last_name;
    _phoneNum.text = [SharedManager shareManager].user.phone;
    
    isCC = true;
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag > 10) {
        [self animateTextField: textField up: YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag > 10) {
        [self animateTextField: textField up: NO];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)sliderPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}
- (IBAction)ccPressed:(id)sender {
    isCC = true;
    [_ccBtn setImage:[UIImage imageNamed:@"radio_button_active.png"] forState:UIControlStateNormal];
    [_payBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    
    _ccDetail.hidden = false;
    
    _submitBankBtn.hidden = true;
    _submitBtn.hidden = false;
    
}

- (IBAction)bankWirePressed:(id)sender {
    isCC = false;
    [_ccBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    [_payBtn setImage:[UIImage imageNamed:@"radio_button_active.png"] forState:UIControlStateNormal];
    
    _ccDetail.hidden = true;
    
    _submitBankBtn.hidden = false;
    _submitBtn.hidden = true;
}

- (IBAction)expiryMonthPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"January", @"Feburary",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 130;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)expiryYearPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"2015", @"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 130;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)submitPressed:(id)sender {
    if([self validateFields]) {
        [CustomLoading showAlertMessage];
        if(isPremium) {
            [self sendBillingCall:@"pre"];
        }
        else {
            [self sendBillingCall:@"amb"];
        }
    }
}

- (IBAction)countryListPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = countryNames;
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        isCountrySelected = true;
        CGFloat f = 130;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.tag = 11;
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)statePressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = stateNames;
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 130;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.tag = 12;
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
    
    NIDropDown *temp = (NIDropDown*)sender;
    if(temp.tag == 11)
    {
        int tag = temp.selectedIndex;
        _stateBtn.enabled = true;
        NSString *STR =_countryPressed.titleLabel.text;
        NSLog(STR);
        
        NSDictionary *statesDict = [countries objectAtIndex:tag];
        
        selectedCountryCode = [statesDict objectForKey:@"code"];
        NSArray *statesArray = [statesDict objectForKey:@"state"];
        stateNames = nil;
        stateCodes = nil;
        
        stateNames = [[NSMutableArray alloc] init];
        stateCodes = [[NSMutableArray alloc] init];
        
        for(int j =0; j<statesArray.count; j++){
            NSDictionary *stateDict = [statesArray objectAtIndex:j];
            
            NSString *stateName = [stateDict objectForKey:@"name"];
            NSString *stateCode = [stateDict objectForKey:@"code"];
            
            [stateCodes addObject:stateCode];
            [stateNames addObject:stateName];
        }
    }
    else if (temp.tag == 12){
        int tag = temp.selectedIndex;
        
        selectedStateCode = [stateCodes objectAtIndex:tag];
    }
    
    
}
-(BOOL) validateFields {
    
    if(_countryPressed.titleLabel.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please select your country or state of residence" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_firstName.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"First Name field cannot be empty." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_lastName.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Last Name field cannot be empty." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_address1.text.length < 1 || _address2.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Address field cannot be empty." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_city.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please specify your city." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_zip.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter your Zip Code." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (selectedStateCode.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter your State of residence" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_phoneNum.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Phone Number field cannot be empty." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    if(isCC) {
        if (_cardHolderName.text.length < 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter Credit Card Holder name" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return false;
        }
        else if (_ccvNum.text.length < 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter Credit Card CCV Number" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return false;
        }
        else if (_cardNum.text.length < 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter Credit Card Number" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return false;
        }
        else if (_expiryBtn.titleLabel.text.length < 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter Credit Card Expiry Month" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return false;
        }
        else if (_ExpiryYearBtn.titleLabel.text.length < 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please enter Credit Card Expiry Year" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return false;
        }
    }
    
    if(_companyName.text.length > 0) {
        if(![_ssn.text isEqualToString:_ssnRepeat.text]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"SSN does not match. Please re enter these values" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return false;
        }
    }
    
    
    
    
    return true;
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

-(void) sendBillingCall : (NSString*)type {
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_UPGRADE forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"imatrix_member_id"];
    [postParams setObject:type forKey:@"type"];
    
    if(isCC) {
        [postParams setObject:@"card" forKey:@"payment_method"];
        [postParams setObject:_cardHolderName.text forKey:@"card_holder_name"];
        [postParams setObject:_cardNum.text forKey:@"card_number"];
        
        NSString *monthName = _expiryBtn.titleLabel.text;
        NSString *monthNumber = @"";
        
        if([monthName isEqualToString:@"January"]) {
            monthNumber = @"01";
        }
        else if ([monthName isEqualToString:@"Feburary"]) {
            monthNumber = @"02";
        }
        else if ([monthName isEqualToString:@"March"]) {
            monthNumber = @"03";
        }
        else if ([monthName isEqualToString:@"April"]) {
            monthNumber = @"04";
        }
        else if ([monthName isEqualToString:@"May"]) {
            monthNumber = @"05";
        }
        else if ([monthName isEqualToString:@"June"]) {
            monthNumber = @"06";
        }
        else if ([monthName isEqualToString:@"July"]) {
            monthNumber = @"07";
        }
        else if ([monthName isEqualToString:@"August"]) {
            monthNumber = @"08";
        }
        else if ([monthName isEqualToString:@"September"]) {
            monthNumber = @"09";
        }
        else if ([monthName isEqualToString:@"October"]) {
            monthNumber = @"10";
        }
        else if ([monthName isEqualToString:@"November"]) {
            monthNumber = @"11";
        }
        else if ([monthName isEqualToString:@"December"]) {
            monthNumber = @"12";
        }
        
        [postParams setObject:monthNumber forKey:@"expiry_month"];
        [postParams setObject:_ExpiryYearBtn.titleLabel.text forKey:@"expiry_year"];
        [postParams setObject:_ccvNum.text forKey:@"card_ccv_digits"];
    }
    else {
        [postParams setObject:@"bank_draft" forKey:@"payment_method"];
    }
    
    
    [postParams setObject:_companyName.text forKey:@"company"];
    [postParams setObject:_ssn.text forKey:@"ssn_or_tex_id"];
    [postParams setObject:_city.text forKey:@"ship_city"];
    [postParams setObject:_zip.text forKey:@"ship_zip"];
    [postParams setObject:selectedStateCode forKey:@"ship_state"];
    [postParams setObject:selectedCountryCode forKey:@"ship_country"];
    [postParams setObject:_address1.text forKey:@"ship_street_address"];
    [postParams setObject:_address2.text forKey:@"ship_street_address2"];
    [postParams setObject:[NSString stringWithFormat:@"%d",fun_pack] forKey:@"fun_pack"];
    [postParams setObject:_countryPressed.titleLabel.text forKey:@"country"];
    [postParams setObject:_city.text forKey:@"city"];
    [postParams setObject:_zip.text forKey:@"zip"];
    [postParams setObject:_state.text forKey:@"state"];
    [postParams setObject:_address1.text forKey:@"street_address2"];
    [postParams setObject:_address2.text forKey:@"street_address"];
    
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                
                BOOL redirect = [[result objectForKey:@"ipayout_redirect"] boolValue];
                
                if(redirect){
                    NSString *strLink = [result objectForKey:@"redirect"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strLink]];
                }else{
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Confirmation" message:@"Order has been placed successfully."delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    [[NavigationHandler getInstance] NavigateToHomeScreen];
                    
                }
                
            }
            else {
                if(isCC) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:[result objectForKey:@"error_1"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}
@end
