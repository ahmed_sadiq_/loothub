//
//  InAppPurchaseManager.m
//  SleepTime
//
//  Created by Apple on 23/04/2014.
//  Copyright (c) 2014 TxLabz. All rights reserved.
//

#import "InAppPurchaseManager.h"
#import "CustomLoading.h"
#define kInAppPurchase100PointsProductId @"com.wits.100pointsnew"
#define kInAppPurchase500PointsProductId @"com.wits.500pointsnew"
#define kInAppPurchase1000PointsProductId @"com.wits.220points"

#define kInAppPurchase5TokenProductId @"com.loothub.5tokens"
#define kInAppPurchase25TokenProductId @"com.loothub.25tokens"
#define kInAppPurchase50TokenProductId @"com.loothub.50tokens"
#define kInAppPurchase100TokenProductId @"com.loothub.100tokens"
#define kInAppPurchase250TokenProductId @"com.loothub.250tokens"
#define kInAppPurchase500TokenProductId @"com.loothub.500tokens"


@implementation InAppPurchaseManager

+ (InAppPurchaseManager *)sharedInstance {
    static dispatch_once_t once;
    static InAppPurchaseManager * sharedInstance;
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma Public methods

//
// call this method once on startup
//
- (void)loadStore :(int) type
{
    // restarts any purchases if they were interrupted last time the app was open
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    switch (type) {
        case 0:
            [self request5TokensProductData];
            break;
        case 1:
            [self request25TokensProductData];
            break;
        case 2:
            [self request50TokensProductData];
            break;
        case 3:
            [self request100TokensProductData];
            break;
        case 4:
            [self request250TokensProductData];
            break;
        case 5:
            [self request500TokensProductData];
            break;
            
        default:
            break;
    }
    
}

//
// call this before making a purchase
//
- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

#pragma mark -
#pragma mark FunHub Purchase Methods

- (void)purchase5Tokens
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchase5TokenProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)request5TokensProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchase5TokenProductId ];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)purchase25Tokens
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchase25TokenProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)request25TokensProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchase25TokenProductId ];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)purchase50Tokens
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchase50TokenProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)request50TokensProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchase50TokenProductId ];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)purchase100Tokens
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchase100TokenProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)request100TokensProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchase100TokenProductId ];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)purchase250Tokens
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchase250TokenProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)request250TokensProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchase250TokenProductId ];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)purchase500Tokens
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchase500TokenProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)request500TokensProductData
{
    NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchase500TokenProductId ];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

#pragma mark -
#pragma mark Extra Methods



#pragma mark -
#pragma mark SKProductsRequestDelegate methods

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *products = response.products;
    proUpgradeProduct = [products count] == 1 ? [products firstObject] : nil;
    if (proUpgradeProduct)
    {
        NSLog(@"Product title: %@" , proUpgradeProduct.localizedTitle);
        NSLog(@"Product description: %@" , proUpgradeProduct.localizedDescription);
        NSLog(@"Product price: %@" , proUpgradeProduct.price);
        NSLog(@"Product id: %@" , proUpgradeProduct.productIdentifier);
    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers)
    {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
}

#pragma -
#pragma Purchase helpers

//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchase5TokenProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"5TokensTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchase25TokenProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"25TokensTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchase50TokenProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"50TokensTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchase100TokenProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"100TokensTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchase250TokenProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"250TokensTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchase500TokenProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"500TokensTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId
{
    if ([productId isEqualToString:kInAppPurchase5TokenProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is5TokensBought"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([productId isEqualToString:kInAppPurchase25TokenProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is25TokensBought"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([productId isEqualToString:kInAppPurchase50TokenProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is50TokensBought"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([productId isEqualToString:kInAppPurchase100TokenProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is100TokensBought"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([productId isEqualToString:kInAppPurchase250TokenProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is250TokensBought"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([productId isEqualToString:kInAppPurchase500TokenProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is500TokensBought"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
    if (wasSuccessful)
    {
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];
    }
    else
    {
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
    }
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    [self recordTransaction:transaction.originalTransaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    [CustomLoading DismissAlertMessage];
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        // error!
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}
@end
