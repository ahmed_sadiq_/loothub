//
//  AppDelegate.h
//  Funhub
//
//  Created by Hannan Khan on 11/11/15.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ViewController;
@class HomeVC;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property ( strong , nonatomic ) UINavigationController *navigationController;
@property ( strong , nonatomic ) UIViewController *viewController;
@property int tokensPurchased;
@property (strong, nonatomic) NSString *referralId;

@end

