//
//  HomeVC.m
//  Funhub
//
//  Created by Ahmed Sadiq on 11/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "HomeVC.h"
#import "HomeCell.h"
#import "DrawerVC.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "UserModel.h"
#import "SharedManager.h"
#import "Game.h"
#import "NavigationHandler.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>


@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewWillAppear:(BOOL)animated {
    [self splitArrays];
}

- (void)viewDidLayoutSubviews {
    [self populateMainScroller];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    _isNew = true;
    h = 0;
    
    
    
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer)   userInfo:nil repeats:YES];

    if(![SharedManager shareManager].games.count) {
        [CustomLoading showAlertMessage];
        [self getGameList];
    }
    else if ([SharedManager shareManager].games.count == 0) {
        [CustomLoading showAlertMessage];
        [self getGameList];
    }
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    _mainScroller.userInteractionEnabled = YES;
    _mainScroller.exclusiveTouch = YES;
    
    _mainScroller.canCancelContentTouches = YES;
    _mainScroller.delaysContentTouches = YES;
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        [_tenToWinBtn setImage:[UIImage imageNamed:@"10towin_icon.png"] forState:UIControlStateNormal];
        [_powerUpBtn setImage:[UIImage imageNamed:@"powerup_icon_default.png"] forState:UIControlStateNormal];
    }
    else {
        [_tenToWinBtn setImage:[UIImage imageNamed:@"10towin_icon_active.png"] forState:UIControlStateNormal];
        [_powerUpBtn setImage:[UIImage imageNamed:@"powerup_icon_white_active"] forState:UIControlStateNormal];
    }
    
    BOOL isAgree = [[NSUserDefaults standardUserDefaults] boolForKey:@"hasAgreed"];
    if(!isAgree) {
        
        if(![[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"As a Premium Gamer, you may enter optional tournaments using Game Loot tokens for a chance to win prizes.  IMPORTANT:  Apple, Inc. is in NO way associated with these tournaments and is NOT responsible for the prizes won." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Agree", nil];
            [alert show];
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"hasAgreed"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    
}

- (void) populateMainScroller {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    _mainScroller.contentSize = CGSizeMake(screenWidth*8,100 );
    NSArray *imgNameArray = [[NSArray alloc] initWithObjects:@"lootflyer_banner.png",@"ninjarunner.png",@"trivialoot_banner.png",@"roundballescape_banner.png",@"blueyonder.jpg",@"frozen.png",@"roadBattle.jpg",@"rogueValet.png", nil];
    int x=0;
    
    for(int i=0; i<8; i++){
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, screenWidth, _mainScroller.frame.size.height)];
        
        imgView.image = [UIImage imageNamed:(NSString*)[imgNameArray objectAtIndex:i]];
        
        
        [_mainScroller addSubview:imgView];
        
        x= x+screenWidth;
    }
    
    
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return ![view isKindOfClass:[UIButton class]];
}

-(void) onTimer {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    // Updates the variable h, adding 100 (put your own value here!)
    h += screenWidth;
    
    if(h>=(screenWidth*8)) {
        h = 0;
        _mainScroller.contentOffset = CGPointMake(h, 0);
    }
    else {
        [UIView animateKeyframesWithDuration:1.0 delay:0.0 options:nil animations:^{
            [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
                _mainScroller.contentOffset = CGPointMake(h, 0);
            }];
        } completion:^(BOOL finished) {
            //Completion Block
        }];
    }
    int pageNum = (h/screenWidth);
    _pageController.currentPage = pageNum;
    
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)rewardsPressed:(id)sender {
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature and earn 20 tokens every day. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToTenToWin];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1) {
        NSString *username = [SharedManager shareManager].user.referrer_user_name;
        
        NSString *urlToHit = [NSString stringWithFormat:@"http://%@.playgamesgetpaid.com/CP1/",username];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToHit]];
    }
}

- (IBAction)webViewerBackPressed:(id)sender {
    [self.wView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    self.webViwer.hidden = true;
}

- (IBAction)slideMenuPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)tab1Pressed:(id)sender {
    _isNew = true;
    [_mainTblView reloadData];
    _bar1.hidden = false;
    _bar2.hidden = true;
}

- (IBAction)tab2Pressed:(id)sender {
    _isNew = false;
    [_mainTblView reloadData];
    _bar1.hidden = true;
    _bar2.hidden = false;
}

- (IBAction)powerUpPressed:(id)sender {
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToPowerUp];
    }
}

- (IBAction)banner1Pressed:(id)sender {
    [self reactOnBannerClick:27];
}

- (IBAction)banner2Pressed:(id)sender {
    [self reactOnBannerClick:29];
}

- (IBAction)banner3Pressed:(id)sender {
    [self reactOnBannerClick:31];
}

- (IBAction)banner4Pressed:(id)sender {
    [self reactOnBannerClick:26];
}

- (IBAction)banner5Pressed:(id)sender {
    [self reactOnBannerClick:38];
}
- (IBAction)banner6Pressed:(id)sender {
    [self reactOnBannerClick:40];
}

- (IBAction)banner7Pressed:(id)sender {
    
    [self reactOnBannerClick:32];
}

- (void) reactOnBannerClick : (int) gameID {
    for(int i=0; i<_newestGames.count; i++) {
        Game *gameObj= (Game*) [_newestGames objectAtIndex:i];
        
        if([gameObj.game_id intValue]== gameID) {
            if(gameObj.banner_url.length > 2) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:gameObj.banner_url]];
            }
            else {
                
                [CustomLoading showAlertMessage];
                NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
                NSURL *url = [NSURL URLWithString:urlStr];
                
                NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
                [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
                [postParams setObject:[SharedManager shareManager].user.referrer_user_name forKey:@"user_name"];
                [postParams setObject:gameObj.gln_game_id forKey:@"gln_game_id"];
                
                NSData *postData = [self encodeDictionary:postParams];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:postData];
                
                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
                    
                    [CustomLoading DismissAlertMessage];
                    
                    if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
                    {
                        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        
                        int flag = [[result objectForKey:@"flag"] intValue];
                        
                        if(flag == 1) {
                            gameObj.install_url = [result objectForKey:@"deep_link"];
                            gameObj.banner_url = [result objectForKey:@"deep_link"];
                            
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:gameObj.install_url]];
                            
                        }
                        else {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                    }
                    else{
                        NSLog(@"Error: %@", error);
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }];
            }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_isNew) {
        return _newestGames.count;
    }
    else {
        return _FeaturedGames.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell *cell ;
    
    if (IS_IPAD) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell_ipad" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Game *gameObj;
    if(_isNew) {
      gameObj = (Game*) [_newestGames objectAtIndex:indexPath.row];
    }
    else {
        gameObj = (Game*) [_FeaturedGames objectAtIndex:indexPath.row];
    }
    
    cell.mainImg.layer.cornerRadius = 6.0;
    cell.mainImg.clipsToBounds = YES;
    
    
    [cell.mainImg sd_setImageWithURL:[NSURL URLWithString:gameObj.game_icon]
                      placeholderImage:[UIImage imageNamed:@"loadPhoto.jpg"]];
    
    //cell.mainImg.imageURL = [NSURL URLWithString:gameObj.game_icon];
    //NSURL *url = [NSURL URLWithString:gameObj.game_icon];
    //[[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    
    

    cell.shareBtn.tag = indexPath.row;
    cell.installBtn.tag = indexPath.row;
    
    cell.fbBtn.tag = indexPath.row;
    cell.twitterBtn.tag = indexPath.row;
    cell.mailBtn.tag = indexPath.row;
    
    [cell.fbBtn addTarget:self
                      action:@selector(fbShareBtnPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [cell.twitterBtn addTarget:self
                   action:@selector(twitterShareBtnPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    [cell.mailBtn addTarget:self
                   action:@selector(mailShareBtnPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.shareBtn addTarget:self
               action:@selector(shareBtnPressed:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [cell.installBtn addTarget:self
                      action:@selector(installBtnPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    
    if(gameObj.isOverLayShown) {
        cell.overlayView.hidden = false;
        
        tapper = [[UITapGestureRecognizer alloc]
                  initWithTarget:self action:@selector(handleSingleTap:)];
        tapper.cancelsTouchesInView = NO;
        tapper.delegate = self;
        [cell addGestureRecognizer:tapper];
    }else {
        for (UIGestureRecognizer *recognizer in cell.gestureRecognizers) {
            recognizer.enabled = NO;
        }
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD) {
        return 300;
    }
    else {
        return 160;
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Game *gameObj;
    if(_isNew) {
        gameObj = (Game*) [_newestGames objectAtIndex:indexPath.row];
    }
    else {
        gameObj = (Game*) [_FeaturedGames objectAtIndex:indexPath.row];
    }
    
    [[NavigationHandler getInstance] MoveToHomeDetail:gameObj];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIControl class]]) {
        // we touched a button, slider, or other UIControl
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    for(int i=0; i<[SharedManager shareManager].games.count; i++) {
        Game *gameObj = (Game*) [[SharedManager shareManager].games objectAtIndex:i];
        gameObj.isOverLayShown = false;
    }
    [self splitArrays];
    [self.mainTblView reloadData];
}

#pragma mark - Server Communication Methods

-(void) getGameList {
    
    [SharedManager shareManager].user = [[SharedManager shareManager] loadCustomObjectWithKey:@"userTempModel"];

    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_GET_GAMES forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"imatrix_id"];
    
    NSData *postData = [self encodeDictionary:postParams];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                
                [SharedManager shareManager].sessionToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                
                NSMutableArray *gamesToBeAdded = [[NSMutableArray alloc] init];
                
                NSArray *gamesArray = [result objectForKey:@"games_data"];
                for(int i=0; i<gamesArray.count; i++) {
                    
                    NSDictionary *tempDict = [gamesArray objectAtIndex:i];
                    Game *gameObj = [[Game alloc] init];
                    gameObj.android_download_link = [tempDict objectForKey:@"android_download_link"];
                    gameObj.category_id = [tempDict objectForKey:@"category_id"];
                    gameObj.category_name = [tempDict objectForKey:@"category_name"];
                    gameObj.deep_link = [tempDict objectForKey:@"deep_link"];
                    gameObj.gameDesc = [tempDict objectForKey:@"description"];
                    gameObj.game_icon = [tempDict objectForKey:@"game_icon"];
                    gameObj.game_id = [tempDict objectForKey:@"game_id"];
                    gameObj.gln_game_id = [tempDict objectForKey:@"gln_game_id"];
                    gameObj.game_name = [tempDict objectForKey:@"game_name"];
                    gameObj.isFeatured = [[tempDict objectForKey:@"is_featured"] boolValue];
                    gameObj.game_video_link = [tempDict objectForKey:@"game_video_link"];
                    gameObj.ios_download_link = [tempDict objectForKey:@"ios_download_link"];
                    gameObj.created_date = [tempDict objectForKey:@"created_date"];
                    gameObj.video_thumbnail = [tempDict objectForKey:@"video_thumbnail_link"];
                    gameObj.featured_image = [tempDict objectForKey:@"featured_graphic_link"];
                    gameObj.banner_url = [tempDict objectForKey:@"referral_deep_link"];
                    gameObj.screen_shots = [[NSMutableArray alloc] init];
                    
                    NSArray *screenshots = [tempDict objectForKey:@"screen_shots"];
                    for(int j=0; j<screenshots.count; j++) {
                        NSString *imageUrl = [screenshots objectAtIndex:j];
                        [gameObj.screen_shots addObject:imageUrl];
                    }
                    [gamesToBeAdded addObject:gameObj];
                }
                [SharedManager shareManager].user.user_type = [result objectForKey:@"member_rank"];
                [SharedManager shareManager].user.balance = [[NSUserDefaults standardUserDefaults] objectForKey:@"imatrix_total_tokens"];
                
                [SharedManager shareManager].user.referrer_user_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"referrerName"];
                
                
                [SharedManager shareManager].games = gamesToBeAdded;
                [self splitArrays];
                [_mainTblView reloadData];
                
                int y = _mainTblView.frame.origin.y;
                int height = _mainTblView.frame.size.height;
                
                CGRect screenRect = [[UIScreen mainScreen] bounds];
                CGFloat screenWidth = screenRect.size.width;
                CGFloat screenHeight = screenRect.size.height;
                
                _mainTblView.frame = CGRectMake(0, screenHeight, screenWidth, height);
                
                [UIView animateWithDuration:0.4
                                      delay:0.0
                                    options: UIViewAnimationCurveEaseIn
                                 animations:^{
                                     _mainTblView.frame = CGRectMake(0, y, screenWidth, height);
                                 } 
                                 completion:^(BOOL finished){
                                 }];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verification Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}


#pragma mark - Cell Extended Methods

- (IBAction)fbShareBtnPressed:(id)sender {
    UIButton *tagBtn = (UIButton*)sender;
    
    UIView *superView = [tagBtn superview];
    
    UIView *mainSuperView = [superView superview];
    
    UIView *superDuperView = [mainSuperView superview];
    
    UIImageView *gameImg = [superDuperView viewWithTag:88];
    
    Game *gameObj;
    if(_isNew) {
        gameObj= (Game*) [_newestGames objectAtIndex:tagBtn.tag];
    }
    else {
        gameObj= (Game*) [_FeaturedGames objectAtIndex:tagBtn.tag];
    }
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub.",[SharedManager shareManager].user.user_name];
        
        [controller setInitialText:strToShare];
        [controller addURL:[NSURL URLWithString:gameObj.generated_url]];
        
        [controller addImage:gameImg.image];
        [self presentViewController:controller animated:YES completion:Nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"delete");
                
            } else
                
            {
                NSLog(@"post");
            }            
        };
        controller.completionHandler =myBlock;
        
        
    }else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please configure Facebook Account in Settings. "
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}
- (IBAction)twitterShareBtnPressed:(id)sender {
    UIButton *tagBtn = (UIButton*)sender;
    
    UIView *superView = [tagBtn superview];
    
    UIView *mainSuperView = [superView superview];
    
    UIView *superDuperView = [mainSuperView superview];
    
    UIImageView *gameImg = [superDuperView viewWithTag:88];
    
    Game *gameObj;
    if(_isNew) {
        gameObj= (Game*) [_newestGames objectAtIndex:tagBtn.tag];
    }
    else {
        gameObj= (Game*) [_FeaturedGames objectAtIndex:tagBtn.tag];
    }
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to Loot Hub.\n%@",[SharedManager shareManager].user.user_name, gameObj.generated_url];
        
        [tweetSheet setInitialText:strToShare];
        [tweetSheet addImage:gameImg.image];
        [tweetSheet addURL: [NSURL URLWithString:
                             gameObj.generated_url]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"delete");
            } else
            {
                NSLog(@"post");
            }
        };
        tweetSheet.completionHandler =myBlock;
    }
    else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please configure Twitter Account in Settings. "
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
}
- (IBAction)mailShareBtnPressed:(id)sender {
    UIButton *tagBtn = (UIButton*)sender;
    Game *gameObj;
    if(_isNew) {
        gameObj= (Game*) [_newestGames objectAtIndex:tagBtn.tag];
    }
    else {
        gameObj= (Game*) [_FeaturedGames objectAtIndex:tagBtn.tag];
    }
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        NSString *strToShare = [NSString stringWithFormat:@"%@ has invited you to %@. ",[SharedManager shareManager].user.first_name,gameObj.game_name];
        [controller setSubject:strToShare];
        //[Loot Hub]
        NSString *strBody = [NSString stringWithFormat:@"Hey, join the revolutionary gaming network where you will get paid for playing games! \n%@",gameObj.generated_url];
        
        [controller setMessageBody:strBody isHTML:NO];
        if (controller) {
            [self presentViewController:controller animated:YES completion:NULL];
        }
    }
    else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Mail Configuration Required"
                                              message:@"Please setup your mail account on your device to send invite via email."
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (IBAction)shareBtnPressed:(id)sender {
    UIButton *tagBtn = (UIButton*)sender;
    Game *gameObj;
    if(_isNew) {
        gameObj= (Game*) [_newestGames objectAtIndex:tagBtn.tag];
    }
    else {
        gameObj= (Game*) [_FeaturedGames objectAtIndex:tagBtn.tag];
    }
    
    if(gameObj.generated_url.length > 2) {
        gameObj.isOverLayShown = true;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tagBtn.tag inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [self.mainTblView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.user_name forKey:@"user_name"];
        [postParams setObject:gameObj.game_id forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    
                    gameObj.isOverLayShown = true;
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tagBtn.tag inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.mainTblView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                    gameObj.generated_url = [result objectForKey:@"deep_link"];
                    
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
}
- (IBAction)installBtnPressed:(id)sender {
    UIButton *tagBtn = (UIButton*)sender;
    Game *gameObj;
    if(_isNew) {
        gameObj= (Game*) [_newestGames objectAtIndex:tagBtn.tag];
    }
    else {
        gameObj= (Game*) [_FeaturedGames objectAtIndex:tagBtn.tag];
    }
    if(gameObj.install_url.length < 2 && gameObj.banner_url.length > 2) {
        gameObj.install_url = gameObj.banner_url;
    }
    if(gameObj.install_url.length > 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:gameObj.install_url]];
    }
    else {
        [CustomLoading showAlertMessage];
        NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:SERVER_GET_DEEPLINK forKey:@"method"];
        [postParams setObject:[SharedManager shareManager].user.referrer_user_name forKey:@"user_name"];
        [postParams setObject:gameObj.gln_game_id forKey:@"gln_game_id"];
        
        NSData *postData = [self encodeDictionary:postParams];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                int flag = [[result objectForKey:@"flag"] intValue];
                
                if(flag == 1) {
                    gameObj.install_url = [result objectForKey:@"deep_link"];
                    gameObj.banner_url = [result objectForKey:@"deep_link"];
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:gameObj.install_url]];
                    
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Timeout" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else{
                NSLog(@"Error: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
}

-(void) splitArrays {
    _FeaturedGames = [[NSMutableArray alloc] init];
    _newestGames = [[NSMutableArray alloc] init];
    
    for(int i=0; i<[SharedManager shareManager].games.count; i++) {
        Game *gameObj = [[SharedManager shareManager].games objectAtIndex:i];
        if(gameObj.isFeatured) {
            [_FeaturedGames addObject:gameObj];
        }
        else {
            [_newestGames addObject:gameObj];
        }
    }
    [self sortNewestArray];
}
-(void) sortNewestArray {
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_date" ascending:NO];
    NSArray *orderedArray = [_newestGames sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [_activityIndc stopAnimating];
    _activityIndc.hidden = true;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}
-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    
}

@end
