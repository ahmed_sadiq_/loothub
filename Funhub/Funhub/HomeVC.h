//
//  HomeVC.h
//  Funhub
//
//  Created by Ahmed Sadiq on 11/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface HomeVC : UIViewController <UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate,UIWebViewDelegate,UIAlertViewDelegate> {
    int h;
    UIGestureRecognizer *tapper;
    NSString *deepLinkUrl;
}

@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;
@property (strong, nonatomic) IBOutlet UIImageView *img4;
@property (strong, nonatomic) IBOutlet UIImageView *img5;
@property (strong, nonatomic) IBOutlet UIImageView *img6;
@property (strong, nonatomic) IBOutlet UIImageView *img7;
@property (strong, nonatomic) IBOutlet UIImageView *img8;



@property (weak, nonatomic) IBOutlet UIButton *tenToWinBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroller;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (weak, nonatomic) IBOutlet UITableView *mainTblView;
@property (weak, nonatomic) IBOutlet UIImageView *bar1;
@property (weak, nonatomic) IBOutlet UIImageView *bar2;
@property (weak, nonatomic) IBOutlet UIView *webViwer;
@property (weak, nonatomic) IBOutlet UIWebView *wView;
@property (strong, nonatomic) IBOutlet UIButton *powerUpBtn;

@property (strong, nonatomic) NSMutableArray *FeaturedGames;
@property (strong, nonatomic) NSMutableArray *newestGames;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndc;

@property BOOL isNew;

- (IBAction)rewardsPressed:(id)sender;
- (IBAction)webViewerBackPressed:(id)sender;
- (IBAction)slideMenuPressed:(id)sender;
- (IBAction)tab1Pressed:(id)sender;
- (IBAction)tab2Pressed:(id)sender;
- (IBAction)powerUpPressed:(id)sender;

- (IBAction)banner1Pressed:(id)sender;
- (IBAction)banner2Pressed:(id)sender;
- (IBAction)banner3Pressed:(id)sender;
- (IBAction)banner4Pressed:(id)sender;
- (IBAction)banner5Pressed:(id)sender;




@end
