//
//  Game.h
//  Funhub
//
//  Created by Ahmed Sadiq on 24/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Game : NSObject

@property BOOL isOverLayShown;
@property (strong, nonatomic) NSString *android_download_link;
@property (strong, nonatomic) NSString *category_id;
@property (strong, nonatomic) NSString *category_name;
@property (strong, nonatomic) NSString *created_date;
@property (strong, nonatomic) NSString *deep_link;
@property (strong, nonatomic) NSString *gameDesc;
@property (strong, nonatomic) NSString *game_icon;
@property (strong, nonatomic) NSString *game_id;
@property (strong, nonatomic) NSString *game_name;
@property (strong, nonatomic) NSString *game_video_link;
@property (strong, nonatomic) NSString *ios_download_link;
@property (strong, nonatomic) NSString *video_thumbnail;
@property (strong, nonatomic) NSString *featured_image;
@property (strong, nonatomic) NSString *generated_url;
@property (strong, nonatomic) NSString *install_url;
@property (strong, nonatomic) NSString *banner_url;
@property (strong, nonatomic) NSString *gln_game_id;
@property (strong, nonatomic) NSMutableArray *screen_shots;
@property BOOL isFeatured;
@end
