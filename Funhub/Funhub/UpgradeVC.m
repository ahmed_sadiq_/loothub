//
//  UpgradeVC.m
//  Funhub
//
//  Created by Ahmed Sadiq on 29/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import "UpgradeVC.h"
#import "DrawerVC.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "SharedManager.h"
#import "NavigationHandler.h"
#import <QuartzCore/QuartzCore.h>
@interface UpgradeVC ()

@end

@implementation UpgradeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    isPremium = true;
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        _premiumBtn.enabled = true;
        
        _ambassodorBtn.enabled = true;
    }
    else if ([[SharedManager shareManager].user.user_type isEqualToString:@"p"]) {
        _premiumBtn.enabled = false;
        _ambassodorBtn.enabled = true;
        isPremium = false;
        
        _ambassororBg.image = [UIImage imageNamed:@"game_ambassador_card_active"];
        _premiumBg.image = [UIImage imageNamed:@"premium_gamer_card"];
        
    }
    else if ([[SharedManager shareManager].user.user_type isEqualToString:@"a"]) {
        _premiumBtn.enabled = false;
        _ambassodorBtn.enabled = false;
        _nextBtn.hidden = true;
        
        _ambassororBg.image = [UIImage imageNamed:@"game_ambassador_card"];
        _premiumBg.image = [UIImage imageNamed:@"premium_gamer_card"];
    }
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sliderBtnPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)upgradePremiumUser:(id)sender {
    typeCurrent = @"pre";
    _billingView.hidden = false;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [_billingView addGestureRecognizer:tapper];
    
    _popUpImg.layer.cornerRadius = 5.0;
    _popUpImg.layer.masksToBounds = YES;
    
}

- (IBAction)upgradeAmbassadorUser:(id)sender {
    typeCurrent = @"amb";
    
    _billingView.hidden = false;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [_billingView addGestureRecognizer:tapper];
    
    _popUpImg.layer.cornerRadius = 5.0;
    _popUpImg.layer.masksToBounds = YES;
}
- (IBAction)billingViewClosePressed:(id)sender {
    _billingView.hidden = true;
}

- (IBAction)cardTypePressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Master Card", @"Visa Card",nil];
    NSArray * arrImage = [[NSArray alloc] init];
    arrImage = [NSArray arrayWithObjects:[UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], nil];
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)expiryMonthPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"January", @"Feburary",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)expiryYearPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"2015", @"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)countryPressed:(id)sender {
    NSMutableArray *countries = [NSMutableArray arrayWithCapacity: [[NSLocale ISOCountryCodes] count]];
    
    for (NSString *countryCode in [NSLocale ISOCountryCodes])
    {
        NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
        NSString *country = [[NSLocale currentLocale] displayNameForKey: NSLocaleIdentifier value: identifier];
        [countries addObject: country];
    }
    NSArray * arr = [[NSArray alloc] init];
    arr = countries;;
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 85;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
}

- (IBAction)billingSubmitPressed:(id)sender {
    if([self checkBillingValidity]) {
        [CustomLoading showAlertMessage];
        [self sendBillingCall:typeCurrent];
    }
    else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Form Validation Error"
                                              message:@"All fields must be filled with ONLY valid values."
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)nextPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToUpgrade2:isPremium];
}

- (IBAction)premimumPressed:(id)sender {
    isPremium = true;
    
    _ambassororBg.image = [UIImage imageNamed:@"game_ambassador_card"];
    _premiumBg.image = [UIImage imageNamed:@"premium_gamer_card_active"];
    
    
}

- (IBAction)ambasodorPressed:(id)sender {
    isPremium = false;
    
    _ambassororBg.image = [UIImage imageNamed:@"game_ambassador_card_active"];
    _premiumBg.image = [UIImage imageNamed:@"premium_gamer_card"];
}

-(BOOL) checkBillingValidity {
    if([[_cardTypetn titleLabel] text] < 1) {
        return false;
    }
    else if (_holderNAme.text.length < 1) {
        return false;
    }
    else if (_cardNumTxt.text.length < 1) {
        return false;
    }
    else if (_cvcDigitTxt.text.length < 1) {
        return false;
    }
    else if (_expiryMonthBtn.titleLabel.text.length < 1) {
        return false;
    }
    else if (_expiryYearBtn.titleLabel.text.length < 1) {
        return false;
    }
    else if (_streetAddressTxt.text.length < 1) {
        return false;
    }
    else if (_streetAddressTxt2.text.length < 1) {
        return false;
    }
    else if (_cityTxt.text.length < 1) {
        return false;
    }
    else if (_zipTxt.text.length < 1) {
        return false;
    }
    else if (_countryBtn.titleLabel.text.length < 1) {
        return false;
    }
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    int tag = textField.tag;
    if(tag > 0) {
        [self animateTextField: textField up: YES];
    }
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    int tag = textField.tag;
    if(tag > 0) {
        [self animateTextField: textField up: NO];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

-(void) sendBillingCall : (NSString*)type {
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_UPGRADE forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"imatrix_member_id"];
    [postParams setObject:type forKey:@"type"];
    [postParams setObject:@"card" forKey:@"payment_method"];
    [postParams setObject:@"company" forKey:@"company"];
    [postParams setObject:@"ssn_or_tex_id" forKey:@"ssn_or_tex_id"];
    [postParams setObject:@"ship_city" forKey:@"ship_city"];
    [postParams setObject:@"ship_zip" forKey:@"ship_zip"];
    [postParams setObject:@"ship_state" forKey:@"ship_state"];
    [postParams setObject:@"ship_country" forKey:@"ship_country"];
    [postParams setObject:@"ship_street_address" forKey:@"ship_street_address"];
    [postParams setObject:@"ship_street_address" forKey:@"ship_street_address2"];
    [postParams setObject:@"101" forKey:@"fun_pack"];
    [postParams setObject:_countryBtn.titleLabel.text forKey:@"country"];
    [postParams setObject:_cityTxt.text forKey:@"city"];
    [postParams setObject:_zipTxt.text forKey:@"zip"];
    [postParams setObject:@"state" forKey:@"state"];
    [postParams setObject:_streetAddressTxt2.text forKey:@"street_address2"];
    [postParams setObject:_streetAddressTxt.text forKey:@"street_address"];
    [postParams setObject:_holderNAme.text forKey:@"card_holder_name"];
    [postParams setObject:_cardNumTxt.text forKey:@"card_number"];
    [postParams setObject:_expiryMonthBtn.titleLabel.text forKey:@"expiry_month"];
    [postParams setObject:_expiryYearBtn.titleLabel.text forKey:@"expiry_year"];
    [postParams setObject:_cvcDigitTxt.text forKey:@"card_ccv_digits"];
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                
                
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}
@end
