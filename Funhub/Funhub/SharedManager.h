//
//  SharedManager.h
//  Funhub
//
//  Created by Ahmed Sadiq on 23/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
@interface SharedManager : NSObject
+(SharedManager*)shareManager;

@property (strong, nonatomic) UserModel *user;
@property (strong, nonatomic) NSString *sessionToken;
@property (strong, nonatomic) NSMutableArray *games;
-(void)ResetModel;
- (void)saveCustomObject:(UserModel *)object key:(NSString *)key;
- (UserModel *)loadCustomObjectWithKey:(NSString *)key;
@end
