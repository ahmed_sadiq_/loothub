//
//  NavigationHandler.m
//  Yolo
//
//  Created by Salman Khalid on 13/06/2014.
//  Copyright (c) 2014 Xint Solutions. All rights reserved.
//

#import "NavigationHandler.h"
#import "HomeVC.h"
#import "Constants.h"
#import "ViewController.h"
#import "ShopViewController.h"
#import "HomeDetailVC.h"
#import "UpgradeVC.h"
#import "ProfileVC.h"
#import "ShareViewController.h"
#import "UpgradeScreen2.h"
#import "UpgradeScreen3.h"
#import "tenToWinVC.h"
#import "RESideMenu.h"
#import "LootcoveVCViewController.h"
#import "DEMOLeftMenuViewController.h"
#import "DEMORightMenuViewController.h"


@implementation NavigationHandler

- (id)initWithMainWindow:(UIWindow *)_tempWindow{
    
    if(self = [super init])
    {
        _window = _tempWindow;
    }
    instance = self;
    return self;
}

static NavigationHandler *instance= NULL;

+(NavigationHandler *)getInstance
{
    if (instance == nil) {
        instance = [[super alloc] init];
    }
    
    return instance;
}

-(void)loadFirstVC{
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    HomeVC *homeVC1 = [[HomeVC alloc] init];
    navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
    
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"])
    {
        HomeVC *homeVC1 = [[HomeVC alloc] init];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
        
        DEMOLeftMenuViewController *leftMenuViewController;
        if (IS_IPAD) {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController_Ipad" bundle:nil];
            
        }
        else {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController" bundle:nil];
        }
        DEMORightMenuViewController *rightMenuViewController = [[DEMORightMenuViewController alloc] init];

        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navController
                                                                        leftMenuViewController:leftMenuViewController
                                                                       rightMenuViewController:rightMenuViewController];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
        _window.rootViewController = sideMenuViewController;
        //_window.rootViewController = navController;
        [navController setNavigationBarHidden:YES];
        
    }
    else{
        
        ViewController *_mainVC = [[ViewController alloc] init];
        navController = [[UINavigationController alloc] initWithRootViewController:_mainVC];
        DEMOLeftMenuViewController *leftMenuViewController;
        if (IS_IPAD) {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController_Ipad" bundle:nil];
            
        }
        else {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController" bundle:nil];
        }
        DEMORightMenuViewController *rightMenuViewController = [[DEMORightMenuViewController alloc] init];
        
        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navController
                                                                        leftMenuViewController:leftMenuViewController
                                                                       rightMenuViewController:rightMenuViewController];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
        _window.rootViewController = sideMenuViewController;
        // _window.rootViewController = navController;
        [navController setNavigationBarHidden:YES];
    }
}

-(void)NavigateToHomeScreen{
    
    /*navController = nil;
    if (IS_IPAD) {
        
        HomeVC *homeVC1 = [[HomeVC alloc] initWithNibName:@"HomeVC_iPad" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
    }
    
    else{
        
        HomeVC *homeVC2 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC2];
    }
    
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];*/
    
    navController = nil;
    if (IS_IPAD) {
        
        HomeVC *homeVC1 = [[HomeVC alloc] initWithNibName:@"HomeVC_iPad" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
        
    }
    else if(IS_IPHONE_5){
        HomeVC *homeVC1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
    }
    else{
        
        HomeVC *homeVC2 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC2];
    }
    
    DEMOLeftMenuViewController *leftMenuViewController;
    if (IS_IPAD) {
        leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController_Ipad" bundle:nil];
        
    }
    else {
        leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController" bundle:nil];
    }
    DEMORightMenuViewController *rightMenuViewController = [[DEMORightMenuViewController alloc] init];
    
    
    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navController
                                                                    leftMenuViewController:leftMenuViewController
                                                                   rightMenuViewController:rightMenuViewController];
    sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
    _window.rootViewController = sideMenuViewController;
    
    // _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];

}

-(void)NavigateToLoginScreen{
    
    navController = nil;
    if (IS_IPAD) {
        
        ViewController *LoginVC1 = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:LoginVC1];
    }
    
    else{
        
        ViewController *LoginVC2 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:LoginVC2];
    }
    
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];
    
}
-(void)MoveToShopVC{
    
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        ShopViewController *myBeam = [[ShopViewController alloc] initWithNibName:@"ShopViewController_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        ShopViewController *myBeam = [[ShopViewController alloc] initWithNibName:@"ShopViewController" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToHomeDetail : (Game*) gameObj {
    
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        HomeDetailVC *myBeam = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC_iPad" bundle:nil];
        myBeam.gameModel = gameObj;
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        HomeDetailVC *myBeam = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
        myBeam.gameModel = gameObj;
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToShareVC {
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        ShareViewController *myBeam = [[ShareViewController alloc] initWithNibName:@"ShareViewController_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        ShareViewController *myBeam = [[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToLootCove {
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        LootcoveVCViewController *myBeam = [[LootcoveVCViewController alloc] initWithNibName:@"LootcoveVCViewController_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        LootcoveVCViewController *myBeam = [[LootcoveVCViewController alloc] initWithNibName:@"LootcoveVCViewController" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToPowerUp {
    [navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        tenToWinVC *myBeam = [[tenToWinVC alloc] initWithNibName:@"tenToWinVC_powerUp_iPad" bundle:nil];
        myBeam.isPowerUp = true;
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        tenToWinVC *myBeam = [[tenToWinVC alloc] initWithNibName:@"tenToWinVC" bundle:nil];
        myBeam.isPowerUp = true;
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToTenToWin {
    [navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        tenToWinVC *myBeam = [[tenToWinVC alloc] initWithNibName:@"tenToWinVC_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        tenToWinVC *myBeam = [[tenToWinVC alloc] initWithNibName:@"tenToWinVC_powerUp" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToUpgrades {
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        UpgradeVC *myBeam = [[UpgradeVC alloc] initWithNibName:@"UpgradeVC_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        UpgradeVC *myBeam = [[UpgradeVC alloc] initWithNibName:@"UpgradeVC" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToProfile {
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToUpgrade2 : (BOOL) isPremium {
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        UpgradeScreen2 *myBeam = [[UpgradeScreen2 alloc] initWithNibName:@"UpgradeScreen2_iPad" bundle:nil];
        myBeam.isPremium = isPremium;
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        UpgradeScreen2 *myBeam = [[UpgradeScreen2 alloc] initWithNibName:@"UpgradeScreen2" bundle:nil];
        myBeam.isPremium = isPremium;
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToUpgrade3 : (int) fun_Pack andPreimum : (BOOL)isPremium{
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        UpgradeScreen3 *myBeam = [[UpgradeScreen3 alloc] initWithNibName:@"UpgradeScreen3_iPad" bundle:nil];
        myBeam.fun_pack = fun_Pack;
        myBeam.isPremium = isPremium;
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        UpgradeScreen3 *myBeam = [[UpgradeScreen3 alloc] initWithNibName:@"UpgradeScreen3" bundle:nil];
        myBeam.fun_pack = fun_Pack;
        myBeam.isPremium = isPremium;
        [navController pushViewController:myBeam animated:YES];
    }
}

@end
