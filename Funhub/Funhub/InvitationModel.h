//
//  InvitationModel.h
//  Funhub
//
//  Created by Ahmed Sadiq on 25/04/2016.
//  Copyright © 2016 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvitationModel : NSObject
@property (nonatomic, strong) NSString *imatrix_id;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *invite_url;
@property BOOL Is_active;
@property BOOL Is_accepted;
@property BOOL Expired;

@end
