//
//  ShopViewController.h
//  Funhub
//
//  Created by Ahmed Sadiq on 26/11/2015.
//  Copyright © 2015 Hannan Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface ShopViewController : UIViewController {
}
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *UserId;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;


@property (strong, nonatomic) AppDelegate *appDel;
- (IBAction)sliderPressed:(id)sender;
- (IBAction)buy5TokensPressed:(id)sender;
@end
