//
//  DrawerVC.m
//  HydePark
//
//  Created by Mr on 22/04/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "DrawerVC.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "UIImageView+RoundImage.h"
#import "SharedManager.h"
#import "NavigationHandler.h"
#import "HelpshiftSupport.h"
@interface DrawerVC ()

@end

static DrawerVC *DrawerVC_Instance= NULL;

@implementation DrawerVC

@synthesize _currentState,tag;
float _yLocation;

+(DrawerVC *)getInstance{
    
    if(DrawerVC_Instance == NULL)
    {
        if(IS_IPAD) {
            DrawerVC_Instance = [[DrawerVC alloc] initWithNibName:@"DrawerVC_iPad" bundle:nil];
        }
        else {
            DrawerVC_Instance = [[DrawerVC alloc] initWithNibName:@"DrawerVC" bundle:nil];
        }
        
        _yLocation = 0.0f;
    }
    return DrawerVC_Instance;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        _currentState = OFF_HIDDEN;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (IS_IPHONE_6) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 667);
    }else if (IS_IPHONE_6_PLUS){
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 736);
    }
    
    DrawerVC_Instance = self;
    [profileImg roundImageCorner];
    
    NSString *profileUrlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_profile"];
    
    NSURL* url = [NSURL URLWithString:profileUrlStr];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,
                                               NSData * data,
                                               NSError * error) {
                               if (!error){
                                   profileImg.image = [UIImage imageWithData:data];
                                   [profileImg roundImageCorner];
                                   // do whatever you want with image
                               }
                               
                           }];
    
    
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)AddInView:(UIView *)_parentView{
    parentView = _parentView;
    if(_currentState == OFF_HIDDEN)
        //        [self.view setFrame:CGRectMake(parentView.frame.size.width, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
        if (IS_IPAD) {
            [self.view setFrame:CGRectMake(-424, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
        }else{
            
            [self.view setFrame:CGRectMake(-280, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            if (IS_IPHONE_6) {
                [self.view setFrame:CGRectMake(-285, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
    [_parentView addSubview:self.view];
    
}
-(void)ShowInView{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    if(_currentState == OFF_HIDDEN)
    {
        [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            usernameLbl.text = [NSString stringWithFormat:@"USER NAME : %@",[SharedManager shareManager].user.user_name];
            usernameIdLbl.text = [NSString stringWithFormat:@"ID : %@",[SharedManager shareManager].user.imatrix_id] ;
            userBalLbl.text = [SharedManager shareManager].user.balance;
            _currentState = ON_SCREEN;
            [self.view setFrame:CGRectMake(0, _yLocation, screenWidth, parentView.frame.size.height-_yLocation)];
            
            [parentView bringSubviewToFront:self.view];
            
            
        } completion:^(BOOL done){
            dimmer.hidden = false;
        }];
    }
    
    else if(_currentState == ON_SCREEN)
    {
        [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            _currentState = OFF_HIDDEN;
            if (IS_IPAD) {
                [self.view setFrame:CGRectMake(-screenWidth, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            }else{
                [self.view setFrame:CGRectMake(-screenWidth, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
                if (IS_IPHONE_6) {
                    [self.view setFrame:CGRectMake(-screenWidth , _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
                }
            }
            dimmer.hidden = true;
            
        } completion:nil];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)generalAction:(id)sender{
    
    [self ShowInView];
}

- (IBAction)logTicketPressed:(id)sender {
    
    //[[NavigationHandler getInstance] NavigateToHomeScreen];
    [self ShowInView];
}

- (IBAction)appSettngs:(id)sender {
    //[[NavigationHandler getInstance] NavigateToLoginScreen];
}

- (IBAction)homePressed:(id)sender {
    [[NavigationHandler getInstance] NavigateToHomeScreen];
}

- (IBAction)logOutPressed:(id)sender {
    
    [CustomLoading showAlertMessage];
    [self logoutUser];
}

- (IBAction)towinPressed:(id)sender {
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature and earn 20 tokens every day. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToTenToWin];
    }
}

- (IBAction)powerUpPressed:(id)sender {
//    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature and earn 20 tokens every day. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
//        [alert show];
//    }
//    else {
//        [[NavigationHandler getInstance] MoveToTenToWin];
//    }
    [[NavigationHandler getInstance] MoveToPowerUp];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1) {
        NSString *username = [SharedManager shareManager].user.referrer_user_name;
        
        NSString *urlToHit = [NSString stringWithFormat:@"http://%@.playgamesgetpaid.com/CP1/",username];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToHit]];
    }
}

- (IBAction)shopPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToShopVC];
}

- (IBAction)sliderPressed:(id)sender {
    [self ShowInView];
}

- (IBAction)lootCovePressed:(id)sender {
    [[NavigationHandler getInstance] MoveToLootCove];
}

- (IBAction)sharePressed:(id)sender {
    [[NavigationHandler getInstance] MoveToShareVC];
}
- (IBAction)upgradesPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToUpgrades];
}
- (IBAction)profilePressed:(id)sender {
    [[NavigationHandler getInstance] MoveToProfile];
}

- (IBAction)backPressed:(id)sender {
    [self ShowInView];
}
- (IBAction)helpPressed:(id)sender {
    [HelpshiftSupport showFAQs:self withOptions:nil];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self ShowInView];
}

-(void) logoutUser {
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_LOGOUT forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].sessionToken forKey:@"session_token"];
    
    NSData *postData = [self encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imatrix_total_tokens"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"country"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"referrerName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if(flag == 1) {
                
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[SharedManager shareManager] ResetModel];
                
                [[NavigationHandler getInstance] NavigateToLoginScreen];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[SharedManager shareManager] ResetModel];
                
                [[NavigationHandler getInstance] NavigateToLoginScreen];
            }
        }
        else{
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conection Error" message:@"Please retry after some time" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}


@end
