//
//  tenToWinVC.m
//  Funhub
//
//  Created by Ahmed Sadiq on 19/04/2016.
//  Copyright © 2016 Hannan Khan. All rights reserved.
//

#import "tenToWinVC.h"
#import "SharedManager.h"
#import "DrawerVC.h"
#import "CustomLoading.h"
#import "Constants.h"
#import "UserShareModel.h"
#import "Person.h"
#import "InvitationModel.h"

#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <QuartzCore/QuartzCore.h>

#define   DEGREES_TO_RADIANS(degrees)  ((3.14159265359 * degrees)/ 180)

@interface tenToWinVC ()

@end

#if defined(DEBUG) && DEBUG

@interface FLAnimatedImage (Private)
@property (nonatomic, weak) id debug_delegate;
@end

@implementation FLAnimatedImage (Private)
@dynamic debug_delegate;
@end

@interface FLAnimatedImageView (Private)
@property (nonatomic, weak) id debug_delegate;
@end

@implementation FLAnimatedImageView (Private)
@dynamic debug_delegate;
@end

#endif

@implementation tenToWinVC
@synthesize accountsList;



#pragma mark - Header and Blink Animation

-(void) StartTimer
{
    uiImageTransitionState = 1;
    rotateImageTransitionState = 1;
    _flyingCatcher.hidden = true;
    self.invitationLbl.hidden = true;
    self.invitationLbl.text = [NSString stringWithFormat:@"You have %d accepted invitation !",totalActiveRecords];
    
    self.liveInviteLbl.text = [NSString stringWithFormat:@"You have %d live invitation !",totalRecords];
    self.timerLbl.text = @"";
    self.timerLbl.hidden = false;
    
    _middleView.hidden = false;
    _timeLeftLbl.hidden = false;
    
    
    if(_isPowerUp) {
        [self getPowerUpStatus];
    }else {
        runTimer =  [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:runTimer forMode:NSDefaultRunLoopMode];
    }
    
    
}


-(void) timerTick
{
    if(_isPowerUp) {
        
        if(secondsLeft > 0 ) {
            secondsLeft -- ;
            int hours = secondsLeft / 3600;
            minutes = (secondsLeft % 3600) / 60;
            seconds = (secondsLeft %3600) % 60;
            self.timerLbl.text = [NSString stringWithFormat:@"%02d : %02d : %02d", hours, minutes, seconds];
            
            uiImageTransitionState++;
            if(uiImageTransitionState>9) {
                uiImageTransitionState=9;
            }
            
            NSString *rotateImageName = [NSString stringWithFormat:@"bg_0%d.png",uiImageTransitionState];
            _mainBg.image = [UIImage imageNamed:rotateImageName];
            
        }
        
        rotateImageTransitionState++;
        if(rotateImageTransitionState>9) {
            [runTimer invalidate];
            runTimer = nil;
            
            indexToTick = _premiumMembers-1;
            allTicks = 0;
            [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(eventToFireAndTickAll:) userInfo:nil repeats:YES];
        }
        
        if(rotateImageTransitionState <10) {
            NSString *rotateImageName = [NSString stringWithFormat:@"circle_new_0%d.png",rotateImageTransitionState];
            _archPathImage.image = [UIImage imageNamed:rotateImageName];
        }
        else {
            NSString *rotateImageName = [NSString stringWithFormat:@"circle_new_%d.png",rotateImageTransitionState];
            _archPathImage.image = [UIImage imageNamed:rotateImageName];
        }
    }
    else{
        if(secondsLeft > 0 ) {
            secondsLeft -- ;
            int hours = secondsLeft / 3600;
            minutes = (secondsLeft % 3600) / 60;
            seconds = (secondsLeft %3600) % 60;
            self.timerLbl.text = [NSString stringWithFormat:@"%02d : %02d : %02d", hours, minutes, seconds];
        }
        if(_invitationLbl.hidden) {
            _invitationLbl.hidden = false;
        }
        else {
            _invitationLbl.hidden = true;
        }
    }
    
    
}

- (void)showZoomInPwerUpFirst {
    _powerUpImg.hidden = false;
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         _powerUpImg.transform=CGAffineTransformMakeScale(4.5, 4.5);
                     }
                     completion:^(BOOL finished){
                         //_powerUpImg.transform=CGAffineTransformIdentity;
                         
                         [UIView animateWithDuration:0.3
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:(void (^)(void)) ^{
                                              _powerUpImg.transform=CGAffineTransformMakeScale(8.0, 8.0);
                                          }
                                          completion:^(BOOL finished){
                                              //_powerUpImg.transform=CGAffineTransformIdentity;
                                              [UIView transitionWithView:_powerUpImg
                                                                duration:0.2
                                                                 options:UIViewAnimationOptionTransitionCrossDissolve
                                                              animations:^{
                                                                  
                                                                  _powerUpImg.hidden = YES;
                                                                  
                                                                  
                                                                  [UIView transitionWithView:_powerUpImg
                                                                                    duration:0.2
                                                                                     options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                  animations:^{
                                                                                      
                                                                                      if (!self.imageView1) {
                                                                                          self.imageView1 = [[FLAnimatedImageView alloc] init];
                                                                                          self.imageView1.contentMode = UIViewContentModeScaleAspectFill;
                                                                                          self.imageView1.clipsToBounds = YES;
                                                                                      }
                                                                                      
                                                                                      
                                                                                      [self.view addSubview:self.imageView1];
                                                                                      //self.imageView1.alpha = 0.3;
                                                                                      CGRect screenRect = [[UIScreen mainScreen] bounds];
                                                                                      CGFloat screenWidth = screenRect.size.width;
                                                                                      CGFloat screenHeight = screenRect.size.height;
                                                                                      
                                                                                      self.imageView1.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
                                                                                      
                                                                                      NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"0800_to1000" withExtension:@"gif"];
                                                                                      [self loadAnimatedImageWithURL:url1 completion:^(FLAnimatedImage *animatedImage) {
                                                                                          
                                                                                          self.imageView1.animatedImage = animatedImage;
                                                                                          
                                                                                          
                                                                                      }];
                                                                                      
                                                                                      
                                                                                      [self.imageView1 setLoopCompletionBlock:^(NSUInteger loopCountRemaining) {
                                                                                          
                                                                                          [_imageView1 stopAnimating];
                                                                                          [_imageView1 removeFromSuperview];
                                                                                          _powerUpImg.hidden = true;
                                                                                          
                                                                                          if (!self.imageView2) {
                                                                                              self.imageView2 = [[FLAnimatedImageView alloc] init];
                                                                                              self.imageView2.contentMode = UIViewContentModeScaleAspectFill;
                                                                                              self.imageView2.clipsToBounds = YES;
                                                                                          }
                                                                                          [self.view addSubview:self.imageView2];
                                                                                          
                                                                                          CGRect screenRect = [[UIScreen mainScreen] bounds];
                                                                                          CGFloat screenWidth = screenRect.size.width;
                                                                                          CGFloat screenHeight = screenRect.size.height;
                                                                                          
                                                                                          self.imageView2.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
                                                                                          
                                                                                          NSURL *url2;
                                                                                          if(!_isFullRocket) {
                                                                                              url2= [[NSBundle mainBundle] URLForResource:@"rocket-halfway2" withExtension:@"gif"];
                                                                                          }
                                                                                          else {
                                                                                              url2= [[NSBundle mainBundle] URLForResource:@"rocket-only" withExtension:@"gif"];
                                                                                          }
                                                                                          [self loadAnimatedImageWithURL:url2 completion:^(FLAnimatedImage *animatedImage) {
                                                                                              
                                                                                              self.imageView2.animatedImage = animatedImage;
                                                                                          }];
                                                                                          
                                                                                          [self.imageView2 setLoopCompletionBlock:^(NSUInteger loopCountRemaining) {
                                                                                              
                                                                                              if(_isFullRocket) {
                                                                                                  [_imageView1 stopAnimating];
                                                                                                  [_imageView1 removeFromSuperview];
                                                                                                  _powerUpImg.hidden = true;
                                                                                                  
                                                                                                  if (!self.imageView2) {
                                                                                                      self.imageView2 = [[FLAnimatedImageView alloc] init];
                                                                                                      self.imageView2.contentMode = UIViewContentModeScaleAspectFill;
                                                                                                      self.imageView2.clipsToBounds = YES;
                                                                                                  }
                                                                                                  [self.view addSubview:self.imageView2];
                                                                                                  
                                                                                                  CGRect screenRect = [[UIScreen mainScreen] bounds];
                                                                                                  CGFloat screenWidth = screenRect.size.width;
                                                                                                  CGFloat screenHeight = screenRect.size.height;
                                                                                                  
                                                                                                  self.imageView2.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
                                                                                                  
                                                                                                  NSURL *url3;
                                                                                                  if(_is1000Gems) {
                                                                                                      url3= [[NSBundle mainBundle] URLForResource:@"1000coins" withExtension:@"gif"];
                                                                                                  }
                                                                                                  else {
                                                                                                      url3= [[NSBundle mainBundle] URLForResource:@"500coins" withExtension:@"gif"];
                                                                                                  }
                                                                                                  
                                                                                                  [self loadAnimatedImageWithURL:url3 completion:^(FLAnimatedImage *animatedImage) {
                                                                                                      
                                                                                                      self.imageView2.animatedImage = animatedImage;
                                                                                                  }];
                                                                                                  
                                                                                                  
                                                                                                  [self.imageView2 setLoopCompletionBlock:^(NSUInteger loopCountRemaining) {
                                                                                                      
                                                                                                      [_imageView2 stopAnimating];
                                                                                                      [_imageView2 removeFromSuperview];
                                                                                                      
                                                                                                      [_wheelImg.layer removeAllAnimations];
                                                                                                      _wheelImg.hidden = true;
                                                                                                      
                                                                                                      int userBalance = [[SharedManager shareManager].user.balance intValue];
                                                                                                      
                                                                                                      
                                                                                                      if(!_is1000Gems) {
                                                                                                          
                                                                                                          int newBal = userBalance + 500;
                                                                                                          
                                                                                                          NSString *newBalance = [NSString stringWithFormat:@"%d",newBal];
                                                                                                          
                                                                                                          [SharedManager shareManager].user.balance = newBalance;
                                                                                                      }
                                                                                                      else {
                                                                                                          
                                                                                                          int newBal = userBalance + 1000;
                                                                                                          
                                                                                                          NSString *newBalance = [NSString stringWithFormat:@"%d",newBal];
                                                                                                          
                                                                                                          [SharedManager shareManager].user.balance = newBalance;
                                                                                                      }
                                                                                                      _balanceAmountLbl.text = [SharedManager shareManager].user.balance;
                                                                                                  }];
                                                                                              }
                                                                                              else {
                                                                                                  [_imageView2 stopAnimating];
                                                                                                  [_imageView2 removeFromSuperview];
                                                                                                  
                                                                                                  [_wheelImg.layer removeAllAnimations];
                                                                                                  _wheelImg.hidden = true;
                                                                                                  
                                                                                                  /*int userBalance = [[SharedManager shareManager].user.balance intValue];
                                                                                                  
                                                                                                  
                                                                                                  if(!_isFullRocket) {
                                                                                                      
                                                                                                      int newBal = userBalance + 500;
                                                                                                      
                                                                                                      NSString *newBalance = [NSString stringWithFormat:@"%d",newBal];
                                                                                                      
                                                                                                      [SharedManager shareManager].user.balance = newBalance;
                                                                                                  }
                                                                                                  else {
                                                                                                      
                                                                                                      int newBal = userBalance + 1000;
                                                                                                      
                                                                                                      NSString *newBalance = [NSString stringWithFormat:@"%d",newBal];
                                                                                                      
                                                                                                      [SharedManager shareManager].user.balance = newBalance;
                                                                                                  }
                                                                                                  
                                                                                                  _balanceAmountLbl.text = [SharedManager shareManager].user.balance;*/
                                                                                              }
                                                                                          }];
                                                                                          
                                                                                      }];
                                                                                      
                                                                                      
                                                                                  }
                                                                                  completion:NULL];
                                                                  
                                                              }
                                                              completion:NULL];
                                          }];
                     }];
}

- (void)eventToFire:(NSTimer*)timer1 {
    
    BOOL timerEnded = false;
    if(allTicks == totalActiveRecords) {
        timerEnded = true;
    }
    if(indexToTick < 10) {
        InvitationModel *iModel = [_recievedInvitationsList objectAtIndex:indexToTick];
        
        UIImageView * imgView = (UIImageView*)[_layerToSlide viewWithTag:indexToTick];
        if(iModel.Is_accepted) {
            imgView.image = [UIImage imageNamed:@"mail_icon_check.png"];
            
            //Play sound Here
            NSString *path = [NSString stringWithFormat:@"%@/soundTwo.wav", [[NSBundle mainBundle] resourcePath]];
            NSURL *soundUrl = [NSURL fileURLWithPath:path];
            
            // Create audio player object and initialize with URL to sound
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
            [_audioPlayer play];
            
            allTicks++;
        }
        indexToTick --;
    }
    
    if((indexToTick < 0) || timerEnded) {
        [timer1 invalidate];
        timer1 = nil;
        
        [NSTimer scheduledTimerWithTimeInterval:secondsLeft target:self selector:@selector(stopTimerGlowing:) userInfo:nil repeats:NO];
        
        [self configureInitialCircularAnimation];
        [self startCircularMovement];
        [self StartTimer];
    }
}

- (void)eventToFireAndTickAll:(NSTimer*)timer1 {
    if(indexToTick >= 10) {
        indexToTick = 9;
    }
    if(indexToTick < 10) {
        
        UIImageView * imgView = (UIImageView*)[_layerToSlide viewWithTag:indexToTick];
        imgView.image = [UIImage imageNamed:@"mail_icon_active.png"];
        
        //Play sound Here
        NSString *path = [NSString stringWithFormat:@"%@/soundTwo.wav", [[NSBundle mainBundle] resourcePath]];
        NSURL *soundUrl = [NSURL fileURLWithPath:path];
        
        // Create audio player object and initialize with URL to sound
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
        [_audioPlayer play];
        
        allTicks++;
        indexToTick --;
    }
    
    
    if((indexToTick < 0)) {
        [timer1 invalidate];
        timer1 = nil;
        
        _wheelImg.hidden = false;
        
        CABasicAnimation *rotation;
        rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        rotation.fromValue = [NSNumber numberWithFloat:0];
        rotation.toValue = [NSNumber numberWithFloat:(2*M_PI)];
        rotation.duration = 3.1; // Speed
        rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
        [_wheelImg.layer addAnimation:rotation forKey:@"Spin"];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(showZoomInPwerUpFirst) userInfo:nil repeats:NO];
    }
}


#pragma mark - Circular Animation

- (void) configureInitialCircularAnimation {
    NSArray *animationFrames = [NSArray arrayWithObjects:
                                [UIImage imageNamed:@"glowing parrot00.png"],
                                //[UIImage imageNamed:@"glowing parrot01.png"],
                                [UIImage imageNamed:@"glowing parrot02.png"],
                                //[UIImage imageNamed:@"glowing parrot03.png"],
                                [UIImage imageNamed:@"glowing parrot04.png"],
                                //[UIImage imageNamed:@"glowing parrot05.png"],
                                [UIImage imageNamed:@"glowing parrot06.png"],
                                //[UIImage imageNamed:@"glowing parrot07.png"],
                                [UIImage imageNamed:@"glowing parrot08.png"],
                                //[UIImage imageNamed:@"glowing parrot09.png"],
                                [UIImage imageNamed:@"glowing parrot10.png"],
                                //[UIImage imageNamed:@"glowing parrot11.png"],
                                [UIImage imageNamed:@"glowing parrot12.png"],
                                //[UIImage imageNamed:@"glowing parrot13.png"],
                                [UIImage imageNamed:@"glowing parrot14.png"],
                                //[UIImage imageNamed:@"glowing parrot15.png"],
                                [UIImage imageNamed:@"glowing parrot16.png"],
                                //[UIImage imageNamed:@"glowing parrot17.png"],
                                [UIImage imageNamed:@"glowing parrot18.png"],
                                //[UIImage imageNamed:@"glowing parrot19.png"],
                                [UIImage imageNamed:@"glowing parrot20.png"],
                                //[UIImage imageNamed:@"glowing parrot21.png"],
                                [UIImage imageNamed:@"glowing parrot22.png"],
                                //[UIImage imageNamed:@"glowing parrot23.png"],
                                [UIImage imageNamed:@"glowing parrot24.png"],
                                //[UIImage imageNamed:@"glowing parrot25.png"],
                                [UIImage imageNamed:@"glowing parrot26.png"],
                                //[UIImage imageNamed:@"glowing parrot27.png"],
                                [UIImage imageNamed:@"glowing parrot28.png"],
                                //[UIImage imageNamed:@"glowing parrot29.png"],
                                [UIImage imageNamed:@"glowing parrot30.png"],
                                //[UIImage imageNamed:@"glowing parrot31.png"],
                                [UIImage imageNamed:@"glowing parrot32.png"],
                                //[UIImage imageNamed:@"glowing parrot33.png"],
                                [UIImage imageNamed:@"glowing parrot34.png"],
                                //[UIImage imageNamed:@"glowing parrot35.png"],
                                [UIImage imageNamed:@"glowing parrot36.png"],
                                //[UIImage imageNamed:@"glowing parrot37.png"],
                                [UIImage imageNamed:@"glowing parrot38.png"],
                                //[UIImage imageNamed:@"glowing parrot39.png"],
                                [UIImage imageNamed:@"glowing parrot40.png"],
                                //[UIImage imageNamed:@"glowing parrot41.png"],
                                [UIImage imageNamed:@"glowing parrot42.png"],
                                //[UIImage imageNamed:@"glowing parrot43.png"],
                                [UIImage imageNamed:@"glowing parrot44.png"],
                                //[UIImage imageNamed:@"glowing parrot45.png"],
                                [UIImage imageNamed:@"glowing parrot46.png"],
                                nil];
    
    _glowingImg.animationImages = animationFrames;
    [_glowingImg startAnimating];
}

- (void) startFullColorCircularMovement {
    
    
    _timePointer.hidden = true;
    _glowingImg.hidden = true;
    _timeOutline.hidden = true;
    
    [_glowingImg stopAnimating];
    
    
    
    NSArray *animationFrames = [NSArray arrayWithObjects:
                                [UIImage imageNamed:@"countdown animation00.png"],
                                //[UIImage imageNamed:@"countdown animation01.png"],
                                [UIImage imageNamed:@"countdown animation02.png"],
                                //[UIImage imageNamed:@"countdown animation03.png"],
                                [UIImage imageNamed:@"countdown animation04.png"],
                                //[UIImage imageNamed:@"countdown animation05.png"],
                                [UIImage imageNamed:@"countdown animation06.png"],
                                //[UIImage imageNamed:@"countdown animation07.png"],
                                [UIImage imageNamed:@"countdown animation08.png"],
                                //[UIImage imageNamed:@"countdown animation09.png"],
                                [UIImage imageNamed:@"countdown animation10.png"],
                                //[UIImage imageNamed:@"countdown animation11.png"],
                                [UIImage imageNamed:@"countdown animation12.png"],
                                //[UIImage imageNamed:@"countdown animation13.png"],
                                [UIImage imageNamed:@"countdown animation14.png"],
                                //[UIImage imageNamed:@"countdown animation15.png"],
                                [UIImage imageNamed:@"countdown animation16.png"],
                                //[UIImage imageNamed:@"countdown animation17.png"],
                                [UIImage imageNamed:@"countdown animation18.png"],
                                //[UIImage imageNamed:@"countdown animation19.png"],
                                [UIImage imageNamed:@"countdown animation20.png"],
                                //[UIImage imageNamed:@"countdown animation21.png"],
                                [UIImage imageNamed:@"countdown animation22.png"],
                                //[UIImage imageNamed:@"countdown animation23.png"],
                                [UIImage imageNamed:@"countdown animation24.png"],
                                //[UIImage imageNamed:@"countdown animation25.png"],
                                [UIImage imageNamed:@"countdown animation26.png"],
                                //[UIImage imageNamed:@"countdown animation27.png"],
                                [UIImage imageNamed:@"countdown animation28.png"],
                                //[UIImage imageNamed:@"countdown animation29.png"],
                                [UIImage imageNamed:@"countdown animation30.png"],
                                //[UIImage imageNamed:@"countdown animation31.png"],
                                [UIImage imageNamed:@"countdown animation32.png"],
                                //[UIImage imageNamed:@"countdown animation33.png"],
                                [UIImage imageNamed:@"countdown animation34.png"],
                                //[UIImage imageNamed:@"countdown animation35.png"],
                                [UIImage imageNamed:@"countdown animation36.png"],
                                //[UIImage imageNamed:@"countdown animation37.png"],
                                [UIImage imageNamed:@"countdown animation38.png"],
                                //[UIImage imageNamed:@"countdown animation39.png"],
                                [UIImage imageNamed:@"countdown animation40.png"],
                                //[UIImage imageNamed:@"countdown animation41.png"],
                                [UIImage imageNamed:@"countdown animation42.png"],
                                //[UIImage imageNamed:@"countdown animation43.png"],
                                [UIImage imageNamed:@"countdown animation44.png"],
                                //[UIImage imageNamed:@"countdown animation45.png"],
                                [UIImage imageNamed:@"countdown animation46.png"],
                                //[UIImage imageNamed:@"countdown animation47.png"],
                                [UIImage imageNamed:@"countdown animation48.png"],
                                nil];
    
    _colorRing.animationImages = animationFrames;
    _colorRing.animationDuration = 2;
    _colorRing.animationRepeatCount = 1;
    [_colorRing startAnimating];
}

/*UIBezierPath *first_path = [UIBezierPath bezierPathWithArcCenter:pt radius:_timeOutline.frame.size.width/2 startAngle:-M_PI_2 endAngle:M_PI/3 clockwise:YES].CGPath;
 
 
 UIBezierPath *second_path = [UIBezierPath bezierPathWithArcCenter:pt radius:_timeOutline.frame.size.width/2 startAngle:M_PI/3 endAngle:M_PI clockwise:YES].CGPath;
 
 UIBezierPath *third_path = [UIBezierPath bezierPathWithArcCenter:pt radius:_timeOutline.frame.size.width/2 startAngle:M_PI endAngle:2 * M_PI - M_PI_2 clockwise:YES].CGPath;*/

- (void) startCircularMovement {
    // Set up path movement
    pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationCubicPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.repeatCount = INFINITY;
    //pathAnimation.rotationMode = @"auto";
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    pathAnimation.duration = 1.35;
    
    // Create a circle path
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGRect circleContainer = CGRectMake(5, 9, 200, 200); // create a circle from this square, it could be the frame of an UIView
    CGPathAddEllipseInRect(curvedPath, NULL, circleContainer);
    
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    
    [self.timePointer.layer addAnimation:pathAnimation forKey:@"myCircleAnimation"];
}

- (void)rotateImage:(NSTimer*)timer1 {
    
    _timePointer.hidden = false;
    
    
    [_glowingImg stopAnimating];
    
    _timeOutline.hidden = false;
    _timeOutline.image = [UIImage imageNamed:@"timer_full.png"];
    
    _glowingImg.hidden = false;
    
    colorRingFrame = CGRectMake(_colorRing.frame.origin.x, _colorRing.frame.origin.y, _colorRing.frame.size.width, _colorRing.frame.size.height);
    
    [_colorRing stopAnimating];
    _colorRing.image = nil;
    [_colorRing.layer removeAllAnimations];
    if(winner == 0) {
        [_colorRing removeFromSuperview];
    }
    
    NSArray *animationFrames = [NSArray arrayWithObjects:
                                [UIImage imageNamed:@"rotating parrot020.png"],
                                //[UIImage imageNamed:@"rotating parrot021.png"],
                                [UIImage imageNamed:@"rotating parrot022.png"],
                                //[UIImage imageNamed:@"rotating parrot023.png"],
                                [UIImage imageNamed:@"rotating parrot024.png"],
                                //[UIImage imageNamed:@"rotating parrot025.png"],
                                [UIImage imageNamed:@"rotating parrot026.png"],
                                //[UIImage imageNamed:@"rotating parrot027.png"],
                                [UIImage imageNamed:@"rotating parrot028.png"],
                                //[UIImage imageNamed:@"rotating parrot029.png"],
                                [UIImage imageNamed:@"rotating parrot030.png"],
                                //[UIImage imageNamed:@"rotating parrot031.png"],
                                [UIImage imageNamed:@"rotating parrot032.png"],
                                //[UIImage imageNamed:@"rotating parrot033.png"],
                                [UIImage imageNamed:@"rotating parrot034.png"],
                                //[UIImage imageNamed:@"rotating parrot035.png"],
                                [UIImage imageNamed:@"rotating parrot036.png"],
                                //[UIImage imageNamed:@"rotating parrot037.png"],
                                [UIImage imageNamed:@"rotating parrot038.png"],
                                //[UIImage imageNamed:@"rotating parrot039.png"],
                                [UIImage imageNamed:@"rotating parrot040.png"],
                                //[UIImage imageNamed:@"rotating parrot041.png"],
                                [UIImage imageNamed:@"rotating parrot042.png"],
                                //[UIImage imageNamed:@"rotating parrot043.png"],
                                [UIImage imageNamed:@"rotating parrot044.png"],
                                //[UIImage imageNamed:@"rotating parrot045.png"],
                                [UIImage imageNamed:@"rotating parrot046.png"],
                                //[UIImage imageNamed:@"rotating parrot047.png"],
                                [UIImage imageNamed:@"rotating parrot048.png"],
                                //[UIImage imageNamed:@"rotating parrot049.png"],
                                [UIImage imageNamed:@"rotating parrot050.png"],
                                //[UIImage imageNamed:@"rotating parrot051.png"],
                                [UIImage imageNamed:@"rotating parrot052.png"],
                                //[UIImage imageNamed:@"rotating parrot053.png"],
                                [UIImage imageNamed:@"rotating parrot054.png"],
                                //[UIImage imageNamed:@"rotating parrot055.png"],
                                [UIImage imageNamed:@"rotating parrot056.png"],
                                //[UIImage imageNamed:@"rotating parrot057.png"],
                                [UIImage imageNamed:@"rotating parrot058.png"],
                                //[UIImage imageNamed:@"rotating parrot059.png"],
                                [UIImage imageNamed:@"rotating parrot060.png"],
                                //[UIImage imageNamed:@"rotating parrot061.png"],
                                [UIImage imageNamed:@"rotating parrot062.png"],
                                //[UIImage imageNamed:@"rotating parrot063.png"],
                                [UIImage imageNamed:@"rotating parrot064.png"],
                                //[UIImage imageNamed:@"rotating parrot065.png"],
                                [UIImage imageNamed:@"rotating parrot066.png"],
                                //[UIImage imageNamed:@"rotating parrot067.png"],
                                [UIImage imageNamed:@"rotating parrot068.png"],
                                //[UIImage imageNamed:@"rotating parrot069.png"],
                                [UIImage imageNamed:@"rotating parrot070.png"],
                                //[UIImage imageNamed:@"rotating parrot071.png"],
                                [UIImage imageNamed:@"rotating parrot072.png"],
                                //[UIImage imageNamed:@"rotating parrot073.png"],
                                [UIImage imageNamed:@"rotating parrot074.png"],
                                //[UIImage imageNamed:@"rotating parrot075.png"],
                                [UIImage imageNamed:@"rotating parrot076.png"],
                                //[UIImage imageNamed:@"rotating parrot077.png"],
                                [UIImage imageNamed:@"rotating parrot078.png"],
                                //[UIImage imageNamed:@"rotating parrot079.png"],
                                [UIImage imageNamed:@"rotating parrot080.png"],
                                //[UIImage imageNamed:@"rotating parrot081.png"],
                                [UIImage imageNamed:@"rotating parrot082.png"],
                                //[UIImage imageNamed:@"rotating parrot083.png"],
                                [UIImage imageNamed:@"rotating parrot084.png"],
                                //[UIImage imageNamed:@"rotating parrot085.png"],
                                [UIImage imageNamed:@"rotating parrot086.png"],
                                //[UIImage imageNamed:@"rotating parrot087.png"],
                                [UIImage imageNamed:@"rotating parrot088.png"],
                                //[UIImage imageNamed:@"rotating parrot089.png"],
                                [UIImage imageNamed:@"rotating parrot090.png"],
                                //[UIImage imageNamed:@"rotating parrot091.png"],
                                [UIImage imageNamed:@"rotating parrot092.png"],
                                //[UIImage imageNamed:@"rotating parrot093.png"],
                                [UIImage imageNamed:@"rotating parrot094.png"],
                                //[UIImage imageNamed:@"rotating parrot095.png"],
                                [UIImage imageNamed:@"rotating parrot096.png"],
                                //[UIImage imageNamed:@"rotating parrot097.png"],
                                [UIImage imageNamed:@"rotating parrot098.png"],
                                //[UIImage imageNamed:@"rotating parrot099.png"],
                                [UIImage imageNamed:@"rotating parrot100.png"],
                                //[UIImage imageNamed:@"rotating parrot101.png"],
                                [UIImage imageNamed:@"rotating parrot102.png"],
                                //[UIImage imageNamed:@"rotating parrot103.png"],
                                [UIImage imageNamed:@"rotating parrot104.png"],
                                //[UIImage imageNamed:@"rotating parrot105.png"],
                                [UIImage imageNamed:@"rotating parrot106.png"],
                                //[UIImage imageNamed:@"rotating parrot107.png"],
                                [UIImage imageNamed:@"rotating parrot108.png"],
                                //[UIImage imageNamed:@"rotating parrot109.png"],
                                [UIImage imageNamed:@"rotating parrot110.png"],
                                //[UIImage imageNamed:@"rotating parrot111.png"],
                                [UIImage imageNamed:@"rotating parrot112.png"],
                                //[UIImage imageNamed:@"rotating parrot113.png"],
                                [UIImage imageNamed:@"rotating parrot114.png"],
                                //[UIImage imageNamed:@"rotating parrot115.png"],
                                [UIImage imageNamed:@"rotating parrot116.png"],
                                //[UIImage imageNamed:@"rotating parrot117.png"],
                                [UIImage imageNamed:@"rotating parrot118.png"],
                                //[UIImage imageNamed:@"rotating parrot119.png"],
                                //[UIImage imageNamed:@"rotating parrot120.png"],
                                [UIImage imageNamed:@"rotating parrot121.png"],
                                nil];
    
    _glowingImg.animationImages = animationFrames;
    _glowingImg.animationDuration = 10.0;
    _glowingImg.animationRepeatCount = INFINITY;
    [_glowingImg startAnimating];
    
    NSString *path = [NSString stringWithFormat:@"%@/soundFive.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [_audioPlayer play];
    
    timerFinal = [NSTimer scheduledTimerWithTimeInterval:8.5 target:self selector:@selector(timerFinalCompleted:) userInfo:nil repeats:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(startFlyingNow:) userInfo:nil repeats:NO];
    
}

- (void)stopTimerGlowing:(NSTimer*)timer1 {
    _glowingImg.image = nil;
    [_glowingImg stopAnimating];
    
    [timer1 invalidate];
    timer1 = nil;
    
    [self.timePointer.layer removeAnimationForKey:@"myCircleAnimation"];
    
    self.timeOutline.image = nil;
    
    [self configureRotateAnimation];
    
}

- (void) configureRotateAnimation {
    
    
    
    NSArray *animationFrames = [NSArray arrayWithObjects:
                                [UIImage imageNamed:@"rotating parrot000.png"],
                                //[UIImage imageNamed:@"rotating parrot001.png"],
                                [UIImage imageNamed:@"rotating parrot002.png"],
                                //[UIImage imageNamed:@"rotating parrot003.png"],
                                [UIImage imageNamed:@"rotating parrot004.png"],
                                //[UIImage imageNamed:@"rotating parrot005.png"],
                                [UIImage imageNamed:@"rotating parrot006.png"],
                                //[UIImage imageNamed:@"rotating parrot007.png"],
                                [UIImage imageNamed:@"rotating parrot008.png"],
                                //[UIImage imageNamed:@"rotating parrot009.png"],
                                [UIImage imageNamed:@"rotating parrot010.png"],
                                //[UIImage imageNamed:@"rotating parrot011.png"],
                                [UIImage imageNamed:@"rotating parrot012.png"],
                                //[UIImage imageNamed:@"rotating parrot013.png"],
                                [UIImage imageNamed:@"rotating parrot014.png"],
                                //[UIImage imageNamed:@"rotating parrot015.png"],
                                [UIImage imageNamed:@"rotating parrot016.png"],
                                //[UIImage imageNamed:@"rotating parrot017.png"],
                                [UIImage imageNamed:@"rotating parrot018.png"],
                                [UIImage imageNamed:@"rotating parrot019.png"],
                                nil];
    
    _glowingImg.animationImages = animationFrames;
    _glowingImg.animationDuration = 3.0;
    _glowingImg.animationRepeatCount = 1;
    [_glowingImg startAnimating];
    
    //Stop Lbl flash and timer animation
    [runTimer invalidate];
    runTimer = nil;
    _invitationLbl.hidden = true;
    _timerLbl.hidden = true;
    
    if([self canStartFlyAnimation]) {
        [self startFullColorCircularMovement];
        
        if(IS_IPAD) {
            timerGlowing = [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(rotateImage:) userInfo:nil repeats:NO];
        }
        else {
            timerGlowing = [NSTimer scheduledTimerWithTimeInterval:1.4 target:self selector:@selector(rotateImage:) userInfo:nil repeats:NO];
        }
        
        
    }
    else {
        _timePointer.hidden = true;
        _glowingImg.hidden = true;
    }
}

#pragma mark - Flying Animation
- (BOOL) canStartFlyAnimation {
    
    //for testing
    //return true;
    
    int activeCount;
    for (int i=0; i<_recievedInvitationsList.count; i++) {
        InvitationModel *iModel = [_recievedInvitationsList objectAtIndex:i];
        if(iModel.Is_active) {
            return true;
        }
        else if (iModel.Is_accepted) {
            activeCount++;
        }
    }
    if(activeCount == 10) {
        return true;
    }
    return false;
}

- (void) startFlyingNow :(NSTimer*)timer1  {
    _flyingCatcher.hidden = false;
    //Start this after 2 secs
    timerFlying = [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(configureFlyingAnimation:) userInfo:nil repeats:YES];
}
- (void) configureFlyingAnimation :(NSTimer*)timer1 {
    NSArray *animationFrames = [NSArray arrayWithObjects:
                                [UIImage imageNamed:@"single icon flying00.png"],
                                //[UIImage imageNamed:@"single icon flying01.png"],
                                [UIImage imageNamed:@"single icon flying02.png"],
                                //[UIImage imageNamed:@"single icon flying03.png"],
                                [UIImage imageNamed:@"single icon flying04.png"],
                                //[UIImage imageNamed:@"single icon flying05.png"],
                                [UIImage imageNamed:@"single icon flying06.png"],
                                //[UIImage imageNamed:@"single icon flying07.png"],
                                [UIImage imageNamed:@"single icon flying08.png"],
                                //[UIImage imageNamed:@"single icon flying09.png"],
                                [UIImage imageNamed:@"single icon flying10.png"],
                                //[UIImage imageNamed:@"single icon flying11.png"],
                                [UIImage imageNamed:@"single icon flying12.png"],
                                //[UIImage imageNamed:@"single icon flying13.png"],
                                [UIImage imageNamed:@"single icon flying14.png"],
                                //[UIImage imageNamed:@"single icon flying15.png"],
                                [UIImage imageNamed:@"single icon flying16.png"],
                                //[UIImage imageNamed:@"single icon flying17.png"],
                                [UIImage imageNamed:@"single icon flying18.png"],
                                //[UIImage imageNamed:@"single icon flying19.png"],
                                [UIImage imageNamed:@"single icon flying20.png"],
                                //[UIImage imageNamed:@"single icon flying21.png"],
                                [UIImage imageNamed:@"single icon flying22.png"],
                                [UIImage imageNamed:@"single icon flying23.png"],
                                [UIImage imageNamed:@"single icon flying24.png"],
                                [UIImage imageNamed:@"single icon flying25.png"],
                                nil];
    
    UIImageView *animatedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_middleView.frame.origin.x, _middleView.frame.origin.y, _middleView.frame.size.width/2, _middleView.frame.size.height/2)];
    
    animatedImageView.center = _middleView.center;
    animatedImageView.tag = 99;
    
    [self.flyingCatcher addSubview:animatedImageView];
    
    animatedImageView.animationImages = animationFrames;
    animatedImageView.animationDuration = 2;
    animatedImageView.animationRepeatCount = 1;
    [animatedImageView startAnimating];
    
    int yPos = 40;
    
    if(IS_IPAD) {
        yPos = 20;
    }
    
    [UIView animateWithDuration:1.2
                     animations:^{[animatedImageView setFrame:CGRectMake(_balanceAmountLbl.frame.origin.x, yPos, animatedImageView.frame.size.width, animatedImageView.frame.size.height)];
                     }
                     completion:^ (BOOL finished)
     {
         [animatedImageView stopAnimating];
         animatedImageView.image = nil;
         [animatedImageView removeFromSuperview];
         
         NSString *path = [NSString stringWithFormat:@"%@/coin_collect.wav", [[NSBundle mainBundle] resourcePath]];
         NSURL *soundUrl = [NSURL fileURLWithPath:path];
         
         // Create audio player object and initialize with URL to sound
         _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
         [_audioPlayer play];
         if(pointsAccesingCount < 20) {
             int currentBalance = [_balanceAmountLbl.text intValue];
             currentBalance=currentBalance+2;
             
             [SharedManager shareManager].user.balance = [NSString stringWithFormat:@"%d",currentBalance];
             
             _balanceAmountLbl.text = [NSString stringWithFormat:@"%d",currentBalance];
             
             [[NSUserDefaults standardUserDefaults] setObject:_balanceAmountLbl.text forKey:@"imatrix_total_tokens"];
             [[NSUserDefaults standardUserDefaults] synchronize];
         }
         pointsAccesingCount=pointsAccesingCount+2;
     }];
    
    
}
- (void) timerFinalCompleted :(NSTimer*)timer1 {
    
    if(winner == 0) {
        [self.flyingCatcher removeFromSuperview];
    }
    else {
        NSArray *arr=self.flyingCatcher.subviews;
        
        for (UIView *view1 in arr)
        {
            [view1.layer removeAllAnimations];
            [view1 removeFromSuperview];
            
        }
    }
    
    
    [timerFinal invalidate];
    timerFinal = nil;
    
    [timerFlying invalidate];
    timerFlying = nil;
    
    [_glowingImg stopAnimating];
    [_glowingImg.layer removeAllAnimations];
    
    _glowingImg.hidden = false;
    _glowingImg.image = [UIImage imageNamed:@"rotating parrot121.png"];
    
    _timeLeftLbl.text = @"YOU'VE JUST RECIEVED 20 GAMELOOT TOKENS";
    
    
    if(winner == 1) {
        
        _timeLeftLbl.hidden = false;
        _timeLeftLbl.text = @"TIME LEFT TO THE NEXT DAILY BONUS";
        
        winner = 0;
        
        // Construct URL to sound file
        NSString *path = [NSString stringWithFormat:@"%@/soundOne.wav", [[NSBundle mainBundle] resourcePath]];
        NSURL *soundUrl = [NSURL fileURLWithPath:path];
        
        // Create audio player object and initialize with URL to sound
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
        [_audioPlayer play];
        
        balanceFrame = _balanceAmountLbl.frame;
        [UIView animateWithDuration:1.0
                         animations:^{[_layerToSlide setFrame:CGRectMake(8, [_layerToSlide frame].origin.y, _layerToSlide.frame.size.width, _layerToSlide.frame.size.height)];
                         }
                         completion:^ (BOOL finished)
         {
             if (finished) {
                 indexToTick = 9;
                 allTicks = 0;
                 [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(eventToFire:) userInfo:nil repeats:YES];
             }
         }];
    }
}

#pragma mark - View LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    rotateImgDegree = 40;
    _userNameLbl.text = [NSString stringWithFormat:@"USER NAME : %@",[SharedManager shareManager].user.user_name];
    _idLbl.text = [NSString stringWithFormat:@"ID : %@",[SharedManager shareManager].user.imatrix_id] ;
    
    _flyingCatcher.hidden = true;
    
    _inviteTxt.text= [NSString stringWithFormat:@"IMPORTANT: To prevent spam, you are only allowed to send one invitation at a time."];
    
    _middleView.hidden = true;
    _timeLeftLbl.hidden = true;
    
    _invitationMainView.hidden = true;
    _inviteView.hidden = false;
    
    [self getInvitedList];
    
}

- (void)viewDidLayoutSubviews {
    // VC just laid off its views
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    /*NSTimer *timer;
     NSTimer *timerGlowing;
     NSTimer *timerFlying;
     NSTimer *timerFinal;
     NSTimer *runTimer;*/
    
    [timer invalidate];
    timer = nil;
    
    [timerGlowing invalidate];
    timerGlowing = nil;
    
    [timerFlying invalidate];
    timerFlying = nil;
    
    [timerFinal invalidate];
    timerFinal = nil;
    
    [runTimer invalidate];
    runTimer = nil;
    
    _glowingImg.image = nil;
    _timePointer.image = nil;
    _timeOutline.image = nil;
    _colorRing.image = nil;
    
    
    
    [self.view.layer removeAllAnimations];
    
    NSArray *viewsToRemove = [self.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
}

#pragma mark - Helper
- (void) setInvitedStatus {
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"invited"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (IBAction)drawerPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}
- (IBAction)inviteBackPressed:(id)sender {
    _inviteListView.hidden = true;
}

#pragma mark - Invite List Methods
- (void) getInvitedList {
    _timeLeftLbl.hidden = true;
    _curlArrow.hidden = true;
    
    [CustomLoading showAlertMessage];
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_INVITE_LIST forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"imatrix_id"];
    //[postParams setObject:@"2191601" forKey:@"imatrix_id"];
    
    NSData *postData = [self encodeDictionary:postParams];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"flag"] intValue];
            if(flag == 1) {
                
                NSDictionary *serverTimeDict = [result objectForKey:@"server_time"];
                
                int hoursLeft = [[serverTimeDict objectForKey:@"remaining_hours"] intValue];
                int minsLeft = [[serverTimeDict objectForKey:@"remaining_minutes"] intValue];
                int secLeft = [[serverTimeDict objectForKey:@"remaining_seconds"] intValue];
                
                secondsLeft = (hoursLeft*3600)+(minsLeft*60)+secLeft;
                //secondsLeft = 4;
                
                NSArray *dataArray = [result objectForKey:@"data"];
                _recievedInvitationsList = [[NSMutableArray alloc] init];
                
                
                for(int i=0; i<10; i++) {
                    InvitationModel *iModel = [[InvitationModel alloc] init];
                    
                    [_recievedInvitationsList addObject:iModel];
                }
                
                for(int i=0; i<dataArray.count; i++) {
                    NSDictionary *tempDict = [dataArray objectAtIndex:i];
                    
                    if(tempDict) {
                        InvitationModel *iModel = [_recievedInvitationsList objectAtIndex:i];
                        iModel.Expired = false;
                        
                        int accpetd = [[tempDict objectForKey:@"is_accepted"] intValue];
                        
                        NSString *acceptedStr = [NSString stringWithFormat:@"%d",accpetd];
                        if([acceptedStr isEqualToString:@"1"]) {
                            iModel.Is_accepted = true;
                            
                        }
                        else {
                            iModel.Is_accepted = false;
                        }
                        int ctiveStr = [[tempDict objectForKey:@"is_active"] intValue];
                        
                        NSString *activeStr = [NSString stringWithFormat:@"%d",ctiveStr];
                        
                        if([activeStr isEqualToString:@"1"]) {
                            
                            UIImageView * imgView = (UIImageView*)[_layerToSlide viewWithTag:i];
                            imgView.image = [UIImage imageNamed:@"mail_icon_active.png"];
                            
                            iModel.Is_active = true;
                            totalRecords++;
                        }
                        else {
                            iModel.Is_active = false;
                        }
                        iModel.imatrix_id = [tempDict objectForKey:@"imatrix_id"];
                        iModel.datetime = [tempDict objectForKey:@"datetime"];
                        iModel.invite_url = [tempDict objectForKey:@"invite_url"];
                        
                        if(iModel.Is_accepted) {
                            totalActiveRecords++;
                        }
                        
                        [_recievedInvitationsList replaceObjectAtIndex:i withObject:iModel];
                    }
                }
                
                _curlArrow.hidden = false;
                winner = [[result objectForKey:@"winner"] intValue];
                //winner = 1;
                if(winner == 0) {
                    _timeLeftLbl.hidden = false;
                    
                    _balanceAmountLbl.text = [SharedManager shareManager].user.balance;
                    // Construct URL to sound file
                    NSString *path = [NSString stringWithFormat:@"%@/soundOne.wav", [[NSBundle mainBundle] resourcePath]];
                    NSURL *soundUrl = [NSURL fileURLWithPath:path];
                    
                    // Create audio player object and initialize with URL to sound
                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
                    [_audioPlayer play];
                    
                    balanceFrame = _balanceAmountLbl.frame;
                    [UIView animateWithDuration:1.0
                                     animations:^{[_layerToSlide setFrame:CGRectMake(8, [_layerToSlide frame].origin.y, _layerToSlide.frame.size.width, _layerToSlide.frame.size.height)];
                                     }
                                     completion:^ (BOOL finished)
                     {
                         if (finished) {
                             indexToTick = 9;
                             allTicks = 0;
                             [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(eventToFire:) userInfo:nil repeats:YES];
                         }
                     }];
                }
                
                else {
                    NSString *amount  = [SharedManager shareManager].user.balance ;
                    
                    int points = [amount intValue];
                    points = points;
                    
                    if(points<0)
                    {
                        points = 0;
                    }
                    
                    amount = [NSString stringWithFormat:@"%d",points];
                    _balanceAmountLbl.text = amount;
                    
                    [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(stopTimerGlowing:) userInfo:nil repeats:NO];
                    
                    [self configureInitialCircularAnimation];
                    [self startCircularMovement];
                    [self StartTimer];
                    
                }
                
                
            }
        }
    }];
}

#pragma mark - Invite Methods

- (void) getInviteString {
    [CustomLoading showAlertMessage];
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_INVITE_URL forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"imatrix_id"];
    
    NSData *postData = [self encodeDictionary:postParams];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int flag = [[result objectForKey:@"flag"] intValue];
            
            if(flag == 1) {
                inviteURL = [result objectForKey:@"invite_url"];
                inviteMsg = [result objectForKey:@"invite_url_message"];
                //inviteURL = [NSString stringWithFormat:@"%@ %@",inviteMsg ,inviteURL];
                
                for(int i=0; i<10; i++) {
                    InvitationModel *iModel = [_recievedInvitationsList objectAtIndex:i];
                    
                    if(!iModel.Is_accepted && !iModel.Is_active) {
                        iModel.Is_active = true;
                        
                        UIImageView * imgView = (UIImageView*)[_layerToSlide viewWithTag:i];
                        imgView.image = [UIImage imageNamed:@"mail_icon_active.png"];
                        totalRecords++;
                        
                        self.invitationLbl.text = [NSString stringWithFormat:@"You have %d accepted invitation !",totalActiveRecords];
                        
                        break;
                        
                    }
                }
                
                switch (sharingstate) {
                    case 0:
                        [self configureFBContent];
                        break;
                    case 1:
                        [self getTwitterAccounts];
                        break;
                    case 2:
                        [self getTextAccounts];
                        break;
                    default:
                        break;
                }
                
            }
        }
    }];
}

- (IBAction)viaFb:(id)sender {
    
    if(inviteURL.length>0) {
        [self configureFBContent];
    }
    else {
        sharingstate = 0;
        [self getInviteString];
    }
}
- (void) configureFBContent {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:inviteURL];
    content.contentTitle = @"Play Games & Get Paid!";
    
    FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
    messageDialog.delegate = self;
    [messageDialog setShareContent:content];
    
    if ([messageDialog canShow])
    {
        [messageDialog show];
    }
    else
    {
        // Messenger isn't installed. Redirect the person to the App Store.
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/en/app/facebook-messenger/id454638411?mt=8"]];
    }
}

- (IBAction)viaTwiter:(id)sender {
    
    sharingstate = 1;
    if(inviteURL.length>0) {
        [self getTwitterAccounts];
    }
    else {
        [self getInviteString];
    }
    
    
}

- (IBAction)viaIg:(id)sender {
}

- (IBAction)viaText:(id)sender {
    sharingstate = 2;
    if(inviteURL.length>0) {
        [self getTextAccounts];
    }
    else {
        [self getInviteString];
    }
}

- (IBAction)InviteCancelPressed:(id)sender {
    _invitationMainView.hidden = true;
}

- (IBAction)sendInvitePressed:(id)sender {
    _invitationMainView.hidden = false;
    // border radius
    [self.inviteInnerView.layer setCornerRadius:10.0f];
    
    // border
    [self.inviteInnerView.layer setBorderColor:[UIColor colorWithRed:0.501 green:0.6 blue:0.388 alpha:1].CGColor];
    [self.inviteInnerView.layer setBorderWidth:1.5f];
    
    _inviteTxt.text= [NSString stringWithFormat:@"IMPORTANT: To prevent spam, you are only allowed to send one invitation at a time."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

#pragma mark - Server Communication Helpher Method

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Facebook Dialog Methods

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"didCompleteWithResults");
    [self setInvitedStatus];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError ::: %@" , error);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"sharerDidCancel");
    [self setInvitedStatus];
}

#pragma mark - Text Share Methods

-(void)getTextAccounts {
    self.usersText = [[NSMutableArray alloc] init];
    
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                NSString *phone;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSMutableArray *contactNumbersArray;
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"person-icon.png"];
                    }
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    
                    Person *pTemp = [[Person alloc] init];
                    pTemp.firstName = contact.givenName;
                    pTemp.lastName = contact.familyName;
                    pTemp.fullName = fullName;
                    pTemp.number = phone;
                    
                    [self.usersText addObject:pTemp];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    _inviteListView.hidden = false;
                    [_inviteListTblView reloadData];
                });
            }
        }
    }];
    
    
}

#pragma mark - Twitter Share Methods

-(void)getTwitterAccounts {
    [CustomLoading showAlertMessage];
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType
                            withCompletionHandler:^(BOOL granted, NSError *error) {
                                
                                if (granted && !error) {
                                    accountsList = [accountStore accountsWithAccountType:accountType];
                                    
                                    myAccount = [accountsList objectAtIndex:0];
                                    [self getTwitterFriendsIDListForThisAccount];
                                }
                                else
                                {
                                    [CustomLoading DismissAlertMessage];
                                    
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
                                                                                    message:@"There are no Twitter accounts configured. You must add or create a Twitter separately."
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil];
                                    [alert show];
                                }
                                
                            }];
}

-(void) getFollowerNameFromID:(NSString *)ID{
    
    
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/lookup.json"];
    NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:ID, @"user_id",nil];
    TWRequest *twitterRequest = [[TWRequest alloc] initWithURL:url
                                                    parameters:p
                                                 requestMethod:TWRequestMethodGET];
    [twitterRequest setAccount:myAccount];
    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            
        }
        NSError *jsonError = nil;
        NSDictionary *friendsdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
        resultFollowersNameList = [friendsdata valueForKey:@"name"];
        for(int i=0; i<resultFollowersNameList.count; i++) {
            UserShareModel *uModel = [_users objectAtIndex:i];
            uModel.fullName = [resultFollowersNameList objectAtIndex:i];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            
            _inviteListView.hidden = false;
            [_inviteListTblView reloadData];
            
            [CustomLoading DismissAlertMessage];
            
        }];
        
        
        
    }];
}

/************* getting followers/friends ID list code start here *******/
// so far we have instnce of current account, that is myAccount //

-(void) getTwitterFriendsIDListForThisAccount{
    
    /*** url for all friends *****/
    // NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/ids.json"];
    
    /*** url for Followers only ****/
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/followers/ids.json"];
    
    NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:myAccount.username, @"screen_name", nil];
    
    TWRequest *twitterRequest = [[TWRequest alloc] initWithURL:url parameters:p requestMethod:TWRequestMethodGET];
    [twitterRequest setAccount:myAccount];
    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResposnse, NSError *error)
     {
         if (error) {
             
         }
         paramString = [[NSMutableString alloc] init];
         NSError *jsonError = nil;
         // Convert the response into a dictionary
         NSDictionary *twitterFriends = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
         
         NSArray *IDlist = [twitterFriends objectForKey:@"ids"];
         _users = [[NSMutableArray alloc] init];
         
         NSInteger count = IDlist.count;
         
         for (int i=0; i<count; i++ ) {
             UserShareModel *uModel = [[UserShareModel alloc] init];
             uModel.userID = [IDlist objectAtIndex:i];
             
             
             
             [paramString appendFormat:@"%@",[IDlist objectAtIndex:i]];
             if ((i <count-2)  && i< 80) {
                 NSString *delimeter = @",";
                 [paramString appendString:delimeter];
                 [_users addObject:uModel];
             }
         }
         [self getFollowerNameFromID:paramString];
     }
     ];
    
}

-(void)postMessageToFriend:(NSString *)ID withName : (NSString*)fullName{
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        
        if (granted && !error) {
            
            NSArray *accountsList = [accountStore accountsWithAccountType:accountType];
            
            NSString *textToInvite = [NSString stringWithFormat:@"%@ %@",inviteMsg ,inviteURL];
            
            int NoOfAccounts = [accountsList count];
            if (NoOfAccounts >0) {
                
                NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/direct_messages/new.json"];
                ACAccount *twitterAccount = [accountsList lastObject];
                
                NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:
                                   ID,                    @"user_id",
                                   textToInvite, @"text",
                                   nil
                                   ];
                SLRequest *postRequest = [SLRequest  requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:p];
                [postRequest setAccount:twitterAccount];
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResposnse, NSError *error){
                    
                    NSError *jsonError = nil;
                    
                    NSDictionary *friendsdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
                    
                    NSLog(@"response value is: %@ %d ",friendsdata,[urlResposnse statusCode]);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setInvitedStatus];
                        _inviteListView.hidden = true;
                        [CustomLoading DismissAlertMessage];
                    });
                    
                }];
            }
        }
    }];
}


#pragma mark - Table View  Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.searchResult count];
    }
    else
    {
        if(sharingstate == 1) {
            return [_users count];
        }
        else {
            return [_usersText count];
        }
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isSearch)
    {
        
        static NSString *MyIdentifier = @"MyReuseIdentifier";
        UITableViewCell *cell = [_searchTblView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        }
        if(sharingstate == 1) {
            UserShareModel *region = [_searchResult objectAtIndex:indexPath.row];
            cell.textLabel.text = region.fullName;
        }
        else {
            Person *region = [_searchResult objectAtIndex:indexPath.row];
            cell.textLabel.text = region.fullName;
        }
        
        return cell;
    }
    else
    {
        static NSString *MyIdentifier = @"MyReuseIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        }
        if(sharingstate == 1) {
            UserShareModel *region = [_users objectAtIndex:indexPath.row];
            cell.textLabel.text = region.fullName;
        }
        else {
            Person *region = [_usersText objectAtIndex:indexPath.row];
            cell.textLabel.text = region.fullName;
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(isSearch) {
        UserShareModel *region = [_searchResult objectAtIndex:indexPath.row];
        
        if(sharingstate == 1) {
            NSString *str = [NSString stringWithFormat:@"%d",[region.userID intValue]];
            [CustomLoading showAlertMessage];
            [self postMessageToFriend:str withName:region.fullName];
        }
        else {
            Person *region = [_searchResult objectAtIndex:indexPath.row];
            controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText])
            {
                NSMutableArray *toRecipients = [[NSMutableArray alloc]init];
                controller.body = [NSString stringWithFormat:@"%@ %@",inviteMsg ,inviteURL];
                [toRecipients addObject:region.number];
                controller.recipients = toRecipients;
                controller.messageComposeDelegate = self;
                
                [self.navigationController presentViewController:controller animated:false completion:nil];
            }
        }
    }
    else{
        UserShareModel *region = [_users objectAtIndex:indexPath.row];
        
        if(sharingstate == 1) {
            NSString *str = [NSString stringWithFormat:@"%d",[region.userID intValue]];
            [CustomLoading showAlertMessage];
            [self postMessageToFriend:str withName:region.fullName];
        }
        else {
            Person *region = [_usersText objectAtIndex:indexPath.row];
            controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText])
            {
                NSMutableArray *toRecipients = [[NSMutableArray alloc]init];
                controller.body = [NSString stringWithFormat:@"%@ %@",inviteMsg ,inviteURL];
                [toRecipients addObject:region.number];
                controller.recipients = toRecipients;
                controller.messageComposeDelegate = self;
                
                [self.navigationController presentViewController:controller animated:false completion:nil];
            }
        }
    }
    
}

#pragma mark - Search Bar Delegates Methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.searchResult removeAllObjects];
    //    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.fullname contains[c] %@", searchText];
    //
    //    self.searchResult = [NSMutableArray arrayWithArray: [self.tableData filteredArrayUsingPredicate:resultPredicate]];
    
    isSearch = true;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[c] %@", searchText];
    
    if(sharingstate == 1) {
        self.searchResult = [NSMutableArray arrayWithArray: [_users filteredArrayUsingPredicate:predicate]];
    }
    else {
        self.searchResult = [NSMutableArray arrayWithArray: [self.usersText filteredArrayUsingPredicate:predicate]];
    }
    
    
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
    
    tableView.frame = _inviteListTblView.frame;
    _searchTblView = _inviteListTblView;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.navigationController.navigationBar.hidden = YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    isSearch = false;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [_inviteListTblView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Message Delegates Methods
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller1 didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
        {
            break;
        }
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"The SMS was not sent.  Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
        {
            [self setInvitedStatus];
            break;
        }
        default:
            break;
    }
    _inviteListView.hidden = true;
    [self dismissViewControllerAnimated:false completion:nil];
    [self.navigationController popViewControllerAnimated:true];
}


/// Even though NSURLCache *may* cache the results for remote images, it doesn't guarantee it.
/// Cache control headers or internal parts of NSURLCache's implementation may cause these images to become uncache.
/// Here we enfore strict disk caching so we're sure the images stay around.
- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}

#pragma mark - Power Up Methods

-(void) getPowerUpStatus {
    
    
    [CustomLoading showAlertMessage];
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URL];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:SERVER_POWER_UP forKey:@"method"];
    [postParams setObject:[SharedManager shareManager].user.imatrix_id forKey:@"member_id"];
    
    NSData *postData = [self encodeDictionary:postParams];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"flag"] intValue];
            if(flag == 1) {
                
                int animation_1 = [[result objectForKey:@"animation1"] intValue];
                
                int animation_2 = [[result objectForKey:@"animation2"] intValue];
                _daysPassed = [[result objectForKey:@"days_pass"] intValue];
                _premiumMembers = [[result objectForKey:@"premium_members"] intValue];
                
                if(animation_1) {
                    _isFullRocket = true;
                    if(_daysPassed <= 10) {
                        _is1000Gems = true;
                    }
                    else {
                        _is1000Gems = false;
                    }
                    [self runPowerUpAnimation];
                }
                else if(animation_2) {
                    _isFullRocket = false;
                    [self runPowerUpAnimation];
                }
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:[result objectForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }];
}

-(void) runPowerUpAnimation {
    runTimer =  [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:runTimer forMode:NSDefaultRunLoopMode];
}

@end
