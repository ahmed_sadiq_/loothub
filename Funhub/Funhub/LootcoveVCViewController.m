//
//  LootcoveVCViewController.m
//  Funhub
//
//  Created by Ahmed Sadiq on 29/04/2016.
//  Copyright © 2016 Hannan Khan. All rights reserved.
//

#import "LootcoveVCViewController.h"
#import "SharedManager.h"
#import "NavigationHandler.h"
#import "DrawerVC.h"

@interface LootcoveVCViewController ()

@end

@implementation LootcoveVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.activityIndicator.hidden = false;
    
    [self.activityIndicator startAnimating];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://gamelootrewards.com/?username=%@&password=%@",[SharedManager shareManager].user.email,[SharedManager shareManager].user.password];
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        [_tenToWinBtn setImage:[UIImage imageNamed:@"10towin_icon.png"] forState:UIControlStateNormal];
    }
    else {
        [_tenToWinBtn setImage:[UIImage imageNamed:@"10towin_icon_active.png"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)sliderPressed:(id)sender {
    [DrawerVC getInstance].tag = 2;
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [_activityIndicator stopAnimating];
    _activityIndicator.hidden = true;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}
-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    
}

- (IBAction)tenToWinPressed:(id)sender {
    if([[SharedManager shareManager].user.user_type isEqualToString:@"f"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You must be Premium Gamer to access this feature and earn 20 tokens every day. Click the Link below to learn more" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Link", nil];
        [alert show];
    }
    else {
        [[NavigationHandler getInstance] MoveToTenToWin];
    }
}
@end
